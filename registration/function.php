<?php

session_start();
require_once "lib/users_class.php";
require_once "lib/database_class.php";
require_once "lib/checkvalid_class.php";
require_once "lib/config_class.php";
require_once "lib/data_class.php";

$valid = new CheckValid();
$config = new Config();
$db = new DataBaseReg();
$data = new Data($db);

if (isset($_POST["edit_user"]))
{
    $users = new Users($db);
    $data = $_POST;
    unset($data["edit_user"]);
    $data["date"] = 0;
    $data["group_global"] = "Без группы";
    $data["year"] = 1970;
    if ($users->editUser($data))
        echo "success";
    else
        echo "error";
}


if (isset($_POST["delete_user"]))
{
    $db = new DataBaseReg();
    $users = new Users($db);
    if ($users->deleteUser($_POST["id"]))
        echo "success";
    else
        echo "error";
}


if (isset($_POST["select_dist"]))
{
    $array = testDate($_POST["date"]);
    if ($array["error"] == "e")
        echo "error";
    else
    {
        $year = getYear() - 1 - date("Y", $array["new_date"]);
        if ($year < 18)
        {
            if (($_POST["sex"] != "Женщина"))
            {
                if ($year < 10)
                    $dist = array(3);
                elseif ($year >= 10 && $year < 12)
                    $dist = array(5);
                elseif ($year >= 12 && $year < 14)
                    $dist = array(10);
                elseif ($year >= 14 && $year < 16)
                    $dist = array(15);
                elseif ($year >= 16 && $year < 18)
                    $dist = array(20);
            }
            else
            {
                if ($year < 10)
                    $dist = array(3);
                elseif ($year >= 10 && $year < 12)
                    $dist = array(5);
                elseif ($year >= 12 && $year < 14)
                    $dist = array(10);
                elseif ($year >= 14 && $year < 16)
                    $dist = array(10);
                elseif ($year >= 16 && $year < 18)
                    $dist = array(15);
            }
        }
        else
        {
            if (($_POST["sex"] == "Женщина"))
                $dist = array(30);
            else
                $dist = array(50);

        }

        echo json_encode($dist);
    }

}

if (isset($_POST["submit_reg"]))
{
    $new_user = array();

    $new_user["fio"] = $_POST["famaly"] . " " . $_POST["name"] . " " . $_POST["ochestvo"];
    $new_user["sex"] = $_POST["sex"];
    $new_user["tel"] = $_POST["tel"];
    $new_user["email"] = $_POST["email"];
    $new_user["date"] = $_POST["date"];
    $new_user["dist"] = $_POST["dist"];
    $new_user["region"] = $_POST["region"];
    $new_user["razr"] = $_POST["sport_razr"];

    //-------TEST_DATA----------------------------------------------------------
    $error = array();
    //-----date--------
    $array = testDate($new_user["date"]);
    $error["date"] = $array["error"];
    $new_user["date"] = $array["new_date"];

    //-----fio--------
    $error["famaly"] = ($valid->validStringData($_POST["famaly"]) ? "s" : "e");
    $error["name"] = ($valid->validStringData($_POST["name"]) ? "s" : "e");
    $error["ochestvo"] = ($valid->validStringData($_POST["ochestvo"]) ? "s" : "e");
    //-----sex--------
    $error["sex"] = ($valid->validStringDataSelect($_POST["sex"]) && ((($new_user["sex"] ==
        "Мужчина") || ($new_user["sex"] == "Женщина"))) ? "s" : "e");
    //-----tel--------
    $error["tel"] = ($valid->validStringData($_POST["tel"]) ? "s" : "e");
    //-----email--------
    $error["email"] = ($valid->validEmail($_POST["email"]) ? "s" : "e");
    //-----dist--------
    $error["dist"] = ($valid->validStringDataSelect($_POST["dist"]) ? "s" : "e");
    //-----region--------
    $error["region"] = ($valid->validStringDataSelect($_POST["region"]) ? "s" : "e");
    //-----razr--------
    $error["razr"] = ($valid->validStringDataSelect($_POST["sport_razr"]) ? "s" :
        "e");
    //-----kapcha--------
    $error["kapcha"] = ($_SESSION["rand"] == $_POST["kapcha"] ? "s" : "e");

    if (($error["date"] == "s") && ($error["famaly"] == "s") && ($error["name"] ==
        "s") && ($error["ochestvo"] == "s") && ($error["sex"] == "s") && ($error["tel"] ==
        "s") && ($error["email"] == "s") && ($error["dist"] == "s") && ($error["region"] ==
        "s") && ($error["razr"] == "s") && ($error["kapcha"] == "s"))
        echo addUser($new_user, getYear());
    else
        echo json_encode($error);
}


function testDate($date)
{
    //-----date--------
    $time_start = strtotime("01-01-1900"); //Дата должна быть от этой даты
    $time_mod = strtotime($date);
    if (($time_mod < $time_start) || ($time_mod > time()) || (!$time_mod))
        $error = "e";
    else
    {
        $error = "s";
        $date = $time_mod;
    }
    return array("error" => $error, "new_date" => $date);
}


function addUser($new_user, $conf_year)
{
    $array = userGroup($new_user["date"], $conf_year, $new_user["sex"]);
    $new_user["group"] = $array["group"];
    $new_user["group_global"] = $array["group_global"];
    $new_user["years"] = $array["years"];
    $new_user["year"] = $conf_year;
    $db = new DataBaseReg();
    $users = new Users($db);
    $new_user["number"] = $users->newNumber($new_user["year"], $new_user["group_global"]);
    //echo $new_user["number"];
    if ($users->addUser($new_user))
    {
        sendMailArray($new_user);
        $success = array(getYear(), $new_user["group_global"]);
        echo json_encode($success);
    }
    else
        echo "error";
}

function userGroup($date, $conf_year, $sex)
{
    $year = $conf_year - 1 - date("Y", $date);
    if ($year <= 18)
    {
        if ($year < 10)
            $group = "1-я";
        elseif ($year >= 10 && $year <= 11)
            $group = "2-я";
        elseif ($year >= 12 && $year <= 13)
            $group = "3-я";
        elseif ($year >= 14 && $year <= 15)
            $group = "4-я";
        elseif ($year >= 16 && $year <= 17)
            $group = "5-я";
        $group_global = "детская";
    }
    else
    {
        if (($_POST["sex"] == "Женщина") && ($year >= 70))
            $group = "9-я";
        else
        {
            if ($year <= 34)
                $group = "1-я";
            elseif ($year >= 35 && $year <= 39)
                $group = "2-я";
            elseif ($year >= 40 && $year <= 44)
                $group = "3-я";
            elseif ($year >= 45 && $year <= 49)
                $group = "4-я";
            elseif ($year >= 50 && $year <= 54)
                $group = "5-я";
            elseif ($year >= 55 && $year <= 59)
                $group = "6-я";
            elseif ($year >= 60 && $year <= 64)
                $group = "7-я";
            elseif ($year >= 65 && $year <= 69)
                $group = "8-я";
            elseif ($year >= 70 && $year <= 74)
                $group = "9-я";
            elseif ($year >= 75 && $year <= 79)
                $group = "10-я";
            elseif ($year >= 80)
                $group = "11-я";
        }
        $group_global = "взрослая";
    }
    return array(
        "group" => $group,
        "group_global" => $group_global,
        "years" => $year);
}

function getYear()
{
    $db = new DataBaseReg();
    $data = new Data($db);
    $year = $data->getData("year");
    $year = strtotime("01-01-" . $year);
    return date("Y", $year);
}

function sendMailArray($result)
{
    $config = new Config();
    $message = "Вы зарегистрировались на участие в марафоне. Ваши данные: <br /><table style='border: 2px solid #000' border='0' cellspacing='0' cellpadding='0'>";
    $message .= "<tr><td>Группа</td><td>" . $result["group"] . "</td></tr>";
    $message .= "<tr><td>ФИО</td><td>" . $result["fio"] . "</td></tr>";
    $message .= "<tr><td>Пол</td><td>" . $result["sex"] . "</td></tr>";
    $message .= "<tr><td>Регион</td><td>" . $result["region"] . "</td></tr>";
    $message .= "<tr><td>Возраст (На 31 декабря текущего года)</td><td>" . $result["years"] .
        "</td></tr>";
    $message .= "<tr><td>Спортивный разряд</td><td>" . $result["razr"] .
        "</td></tr>";
    $message .= "<tr><td>Выбранная дистанция</td><td>" . $result["dist"] .
        "</td></tr>";
    $message .= "<tr><td>№ участника</td><td>" . $result["number"] . "</td></tr>";
    $message .= "<tr><td>Телефон</td><td>" . $result["tel"] . "</td></tr>";
    $message .= "<tr><td>E-mail</td><td>" . $result["email"] . "</td></tr>";
    $message .= "</table>";
    if ($result["group_global"] == "взрослая")
        $type_table = "adults";
    if ($result["group_global"] == "детская")
        $type_table = "children";
    $message .= "<br /><a href='" . $config->address . "participants_" . $result["year"] .
        "_" . $type_table . "'>Зарегистрированные учасники</a>";
    sendMail("Регистрация на участие в марафоне", $message, $result["email"], $config);
}

function sendMail($subject, $message, $to, $config)
{
    $from = $config->admin_email;
    $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
    $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/html; charset=utf-8\r\n";
    return mail($to, $subject, $message, $headers);
}

?>