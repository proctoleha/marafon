<?php 
	require_once "lib/users_class.php";
	require_once "lib/database_class.php";
	
		$db = new DataBaseReg();
		$users =  new Users($db);
		
?>
<!DOCTYPE html>
<html>
<head>
	<title>Участники</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script src="js/regitration.js"></script>
</head>
<body>
	<div id="form_out">
		<h2>Форма вывода списка участников для всех желающих</h2>
		<hr />
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>№ участника</th>
				<th>Группа</th>
				<th>ФИО</th>
				<th>Пол</th>
				<th>Регион</th>
				<th>Возраст (на 31 декабря<br /> пред. года)</th>
				<th>Спортивный разряд</th>
				<th>Дистанция</th>
			<tr>
			<?php
				$result = $users->getAllOnYear($_GET["year"], $_GET["group_global"]);
				if($result === false) echo "Неизвестная ошибка! Попробуйте позже или обратитесь к администрации";
				else {
					for($i = 0; $i < count($result); $i++) {
					   echo "<tr><td>".$result[$i]["number"]."</td><td>".$result[$i]["group"]."</td><td>".$result[$i]["fio"]."</td><td>".$result[$i]["sex"]."</td><td>".$result[$i]["region"]."</td><td>".$result[$i]["years"]."</td><td>".$result[$i]["razr"]."</td><td>".$result[$i]["dist"]."</td></tr>";	
					}					
				}
			?>
		</table>
	</div>
</body>