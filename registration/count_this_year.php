<?php
	require_once "lib/users_class.php";
	require_once "lib/database_class.php";
	require_once "lib/data_class.php";

		$db = new DataBaseReg();
		$users =  new Users($db);
		$data =  new Data($db);
		
		
		$year = getYear($db, $data);
		$children = count($users->getAllOnYear($year, "детская"));
		$adult = count($users->getAllOnYear($year, "взрослая"));
		
	function getYear($db, $data) {
		return date("Y", strtotime("01-01-" . $data->getData("year")));
	}
	
?>

<html lang="ru">
<head>
	<title>Участники</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		body {
			background-color: #e6e6e6;
			font-weight: bold;
			margin: 0;
			padding: 0;
			font-family: 'Calibri', 'arial', sans-serif;
		}
		
		#count_div {
			width: 210px;
			height: 135px;
			padding-left: 10px;
			
		}
	
		.count_uch {
			width: 190px;
			height: 25px;
			border-radius: 5px;
			background-color: #00409c;
			margin-top: 10px;
			padding-top: 5px;
			padding-left: 10px;
		}
		
		.sex_group {
			color: #fff;
			width: 200px;
		}
		
		.count {
			float: right;
			color:  #00409c;
			background-color: #e6e6e6;
			width: 40px;
			height: 20px;
			border-radius: 5px;
			display: inline-block;
			text-align: center;
			margin-right: 8px;
		}
	</style>
</head>
<body>
	<div id="count_div">
		<div class="count_uch">
			<span class="sex_group">Детей:</span>
			<span id=" child" class="count"></span>
		</div>
		<div class="count_uch">
			<span class="sex_group">Взрослых:</span>
			<span id="adults" class="count"></span>
		</div>
	</div>
</body>
</html>