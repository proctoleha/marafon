<?php
	session_start();
	require_once "lib/config_class.php";
	$config = new Config();
	if(($_SESSION["login"] !== $config->admin_name) && ($_SESSION["pass"] !== $config->admin_pass)) {
		Header ("Location: index.php");
		exit;
	}
	
	require_once "lib/users_class.php";
	require_once "lib/database_class.php";
	
		$db = new DataBaseReg();
		$users =  new Users($db);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Учасники</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	<script src="js/regitration.js"></script>
</head>
<body>
	<div id="form_print">
		<h2>Форма вывода списка участников для всех желающих</h2>
		<hr />
		<table  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>Группа</th>
				<th>ФИО</th>
				<th>Пол</th>
				<th>Регион</th>
				<th>Возраст (На 31 декабря текущего года)</th>
				<th>Спортивный разряд</th>
				<th>Выбранная дистанция</th>
				<th>№ участника</th>
				<th>Телефон</th>
				<th>E-mail</th>
			<tr>
			<?php
				$result = $users->getAllOnYear($_GET["year"], $_GET["group_global"]);
				if($result === false) echo "Неизвестная ошибка! Попробуйте позже или обратитесь к администрации";
				else {
					for($i = 0; $i < count($result); $i++) {
						echo "<tr><td>".$result[$i]["group"]."</td><td>".$result[$i]["fio"]."</td><td>".$result[$i]["sex"]."</td><td>".$result[$i]["region"]."</td><td>".$result[$i]["years"]."</td><td>".$result[$i]["razr"]."</td><td>".$result[$i]["dist"]."</td><td>".$result[$i]["number"]."</td><td>".$result[$i]["tel"]."</td><td>".$result[$i]["email"]."</td></tr>";	
					}					
				}
			?>
		</table>
	</div>
	<span id="kol_uch">Всего участников: </span><span>4</span>
</body>