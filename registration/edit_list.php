<!DOCTYPE html>
<html>
<head>
	<title>Страница добавления списка</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script src="js/regitration.js"></script>
</head>
<body>
	<div id="form_out" class="edit_list">
		<h2>Страница добавления списка</h2>
		<hr />
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<th>Название списка</th>
				<th>год для определения группы</th>
				<th>Редактировать</th>
				<th>Удалить</th>
			<tr>
			<tr>
				<td>Список 2015 года</td>
				<td>2014</td>
				<td><img src="img/pansel.png" alt="Ред." /></td>
				<td><img src="img/korz.png" alt="Удалить" /></td>
			</tr>
			<tr>
				<td>Список 2015 года</td>
				<td>2014</td>
				<td><img src="img/pansel.png" alt="Ред." /></td>
				<td><img src="img/korz.png" alt="Удалить" /></td>
			</tr>
			<tr>
				<td>Список 2015 года</td>
				<td>2014</td>
				<td><img src="img/pansel.png" alt="Ред." /></td>
				<td><img src="img/korz.png" alt="Удалить" /></td>
			</tr>
			<tr>
				<td>Список 2015 года</td>
				<td>2014</td>
				<td><img src="img/pansel.png" alt="Ред." /></td>
				<td><img src="img/korz.png" alt="Удалить" /></td>
			</tr>
		</table>
		<form id="reg_form" action="#" method="post">
			<div id="edit_list_form">
				<input type="text" name="list" placeholder="Название списка" />
				<input type="text" name="list_date" placeholder="год для определения группы" />
				<input type="submit" name="edit_list" value="Создать новый список" />
			</div>
		</form>
	</div>
</body>