<?php
	session_start();
	$rand = mt_rand(1000, 9999);
	$_SESSION["rand"] = $rand;
	$im = imageCreateTrueColor(200, 50);
	$white = imageColorAllocate($im, 255, 255, 255);
	imageTtfText($im, 20, -10, 10, 30, $white, "fonts/TOLSH.TTF", $rand);
	header("Content-type: image/png");
	imagePng($im);
	imageDestroy($im);
?>