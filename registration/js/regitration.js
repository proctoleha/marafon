var id_user = "";
var edit_prev = "";
var edit_elements = [];
var glob_id = "";
var year_reg = "";
$(document).ready(function() {
	
	$("#select_date").bind("click", function(e) {
		$("#fc").css("margin-left", e.pageX - 50);
		$("#fc").css("margin-top", e.pageY - 50);
	});
	
	$("#fc").bind("click", function() {
		if($(".reg_form input[name='sex']:checked").val() != "null")
		$.ajax({
			url: "function.php",
			type: "POST",
			data: ({select_dist: "test", date: $("#select_date").val(), sex: $(".reg_form input[name='sex']:checked").val()}),
			dataType: "html",
			success: funcDist
		});
		return false;
	});
	
	$("#dist").bind("focus", function() {
		$(".reg_form input[name='sex']").removeClass("error");
		if($(".reg_form input[name='sex']:checked").val() == "null") $(".reg_form input[name='sex']").addClass("error");
		else $.ajax({
			url: "function.php",
			type: "POST",
			data: ({select_dist: "test", date: $("#select_date").val(), sex: $(".reg_form input[name='sex']:checked").val()}),
			dataType: "html",
			success: funcDist
		});
		return false;
	});
	
});

	function yearSuc() {
		if (confirm("Изменить год регистрации? Все регистрации после изменения будут применять настройки выбраного года")) return true;
		else false;
	}

	function edit_send(id){
		if (confirm("Применить изменения?")) {
			glob_id = id;
			for(i = 1; i < 11; i++) {
				edit_elements[i] = $("#edit_form input[name='edit_pole_" + i + "']").val();
			}
			$.ajax({
				url: "function.php",
				type: "POST",
				data: ({edit_user: "edit", id: id, fio: edit_elements[2], sex: edit_elements[3], tel: edit_elements[9], email: edit_elements[10], dist: edit_elements[7], region: edit_elements[4], razr: edit_elements[6], group: edit_elements[1], years: edit_elements[5], number: edit_elements[8]}),
				dataType: "html",
				success: funcEditSucc
			});
			
			
		}
	};
	
	function funcEditSucc(data, d) {
		if (data == "success") {
			var str = $("#edit_prev").parent().parent();
			for(i = 1; i < 12; i++) {
				str.children("td:nth-child(" + i + ")").html(edit_elements[i]);
			}
			str.children("td:nth-child(11)").html("<img onclick = 'editUser(" + glob_id + ")' class='edit_user' id='edit_" + glob_id + "' src='img/pansel.png' alt='Ред.' />");
		}
		else alert("Данные введены в неверном формате! Исправте и попробуйте снова.");
	};
	
	
	function delUser(id) {
		id_user = id;
		if (confirm("Удалить учасника?")) {
			$.ajax({
				url: "function.php",
				type: "POST",
				data: ({delete_user: "del", id: id_user}),
				dataType: "html",
				success: funcDelSucc
			});
		}
	};
	
	function funcDelSucc(data, d) {
		if(data == "success") {
			var elem = $("#del_" + id_user).parent();
			elem = elem.parent();
			elem.remove();
		}
		else alert ("Не удалось удалить, повторите попытку");
	};
	
	function editUser(id) {
		if(edit_prev != "") $("#edit_prev").parent().parent().html(edit_prev);
		var str = $("#edit_" + id).parent().parent();
		edit_prev = str.html();
		var elem = "";
		for(i = 1; i < 11; i++) {
			elem = str.children("td:nth-child(" + i + ")").html();
			edit_elements[i] = elem;
			str.children("td:nth-child(" + i + ")").html("<input type='text' name='edit_pole_" + i + "' value='" + elem + "' />");
		}
		str.children("td:nth-child(" + i + ")").html("<input onclick='edit_send(" + id + ")' id='edit_prev' type='button' name='edit_user' value='Сохранить' />");
	};
	

	function funcDist(data, d) {
		//alert(data);
		$(".reg_form input[name='date']").removeClass("error");
		if(data == "error") {
			$(".reg_form input[name='date']").addClass("error");
		}
			else {
				$("#dist").html("<option value = 'null'>Выберете дистанцию</option>");
				data = JSON.parse(data);
				for (var id in data){
					$("#dist").append("<option>" + data[id] + " км</option>");			
				}
			};
	};

	function chekDate() {
		$.ajax({
			url: "function.php",
			type: "POST",
			data: ({submit_reg: "submit", famaly: $(".reg_form input[name='famaly']").val(), name: $(".reg_form input[name='name']").val(), ochestvo: $(".reg_form input[name='ochestvo']").val(), sex: $(".reg_form input[name='sex']:checked").val(), tel: $(".reg_form input[name='tel']").val(), email: $(".reg_form input[name='email']").val(), date: $(".reg_form input[name='date']").val(), dist: $("select#dist").val(), region: $("select#region").val(), sport_razr: $("select#razr").val(), kapcha: $(".reg_form input[name='kapcha']").val()}),
			dataType: "html",
			success: funcS
		});
		return false;
	};
	
	function funcS(data, d){
		if(data != "error") {
			data = JSON.parse(data);
			window.location = "success.php?year=" + data[0] + "&selector=" + data[1];
		}
		if(data == "error") alert ("Неизвестная ошибка, обратитесь к администрации");
		data = JSON.parse(data);
		//var error_d = "";
			for (var id in data){
				//error_d = error_d + id + " = " + data[id] + "; ";
				if(data[id] == "s") {
					$(".reg_form input[name='" + id + "']").removeClass("error");
					$("#" + id).removeClass("error");
				}
				else{
					$(".reg_form input[name='" + id + "']").addClass("error");
					$("#" + id).addClass("error");
				}
				
			}
	};