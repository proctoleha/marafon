<?php
	session_start();
	require_once "lib/config_class.php";
	$config = new Config();
	if(($_SESSION["login"] !== $config->admin_name) && ($_SESSION["pass"] !== $config->admin_pass)) {
		Header ("Location: index.php");
		exit;
	}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Марафон</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script src="js/regitration.js"></script>
</head>
<body>
	<a href="year_reg.php">Определить год проведения марафона</a><br />
	<hr />
	<br />
	<form action="registred_form_admin.php" method="get">
			<span>Учасники дети </span>
			<select name="year">
				<option selected>2015</option>
				<option>2016</option>
				<option>2017</option>
				<option>2018</option>
				<option>2019</option>
				<option>2020</option>
				<option>2021</option>
				<option>2022</option>
				<option>2023</option>
				<option>2024</option>
				<option>2025</option>
				<option>2026</option>
				<option>2027</option>
				<option>2028</option>
				<option>2029</option>
				<option>2030</option>
			</select>
			<input type="text" hidden name="group_global" value="детская" />
			<input type="submit" value="Открыть" />
	</form>
	<hr />
	<br />
	<form action="registred_form_admin.php" method="get">
			<span>Учасники взрослые </span>
			<select name="year">
				<option selected>2015</option>
				<option>2016</option>
				<option>2017</option>
				<option>2018</option>
				<option>2019</option>
				<option>2020</option>
				<option>2021</option>
				<option>2022</option>
				<option>2023</option>
				<option>2024</option>
				<option>2025</option>
				<option>2026</option>
				<option>2027</option>
				<option>2028</option>
				<option>2029</option>
				<option>2030</option>
			</select>
			<input type="text" hidden name="group_global" value="взрослая" />
			<input type="submit" value="Открыть" />
	</form>
	<hr />
	<br />
	
</body>