<?php
	session_start();
	require_once "lib/config_class.php";
	$config = new Config();
	if(($_SESSION["login"] !== $config->admin_name) && ($_SESSION["pass"] !== $config->admin_pass)) {
		Header ("Location: index.php");
		exit;
	}

	require_once "lib/users_class.php";
	require_once "lib/database_class.php";
	
		$db = new DataBaseReg();
		$users =  new Users($db);
		$result = $users->getAllOnYear($_GET["year"], $_GET["group_global"]);
		
		require_once 'excel/PHPExcel.php'; // Подключаем библиотеку PHPExcel
		  $phpexcel = new PHPExcel(); // Создаём объект PHPExcel
		  /* Каждый раз делаем активной 1-ю страницу и получаем её, потом записываем в неё данные */
		  $page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
		  $page->setCellValue("A1", "Группа"); 
		  $page->setCellValue("B1", "ФИО"); 
		  $page->setCellValue("C1", "Пол"); 
		  $page->setCellValue("D1", "Регион"); 
		  $page->setCellValue("E1", "Возраст"); 
		  $page->setCellValue("F1", "Спортивный разряд"); 
		  $page->setCellValue("G1", "Выбранная дистанция"); 
		  $page->setCellValue("H1", "№"); 
		  $page->setCellValue("I1", "Телефон"); 
		  $page->setCellValue("J1", "E-mail"); 
		  
		  		  
		$style_wrap = array(
			//рамки
			'borders'=>array(
				//внутренняя
				'allborders'=>array(
					'style'=>PHPExcel_Style_Border::BORDER_THIN,
					'color' => array(
					'rgb'=>'696969'
					)
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
				"wraptext" => true,
			),
		);
		echo $i;
		$page->getStyle('A1:J1')->applyFromArray($style_wrap);
		$page->getStyle('A1:J1')->getAlignment()->setWrapText(true);
		  
		  
			if($result === false) $page->setCellValue("A2", "Неизвестная ошибка! Попробуйте позже или обратитесь к администрации");
				else {
					for($i = 0; $i < count($result); $i++) {
						$page->setCellValueExplicit("A".($i + 2),$result[$i]["group"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValueExplicit("B".($i + 2),$result[$i]["fio"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValueExplicit("C".($i + 2),$result[$i]["sex"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValueExplicit("D".($i + 2),$result[$i]["region"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValue("E".($i + 2),$result[$i]["years"]);
						$page->setCellValueExplicit("F".($i + 2),$result[$i]["razr"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValueExplicit("G".($i + 2),$result[$i]["dist"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValue("H".($i + 2),$result[$i]["number"]);
						$page->setCellValueExplicit("I".($i + 2),$result[$i]["tel"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->setCellValueExplicit("J".($i + 2),$result[$i]["email"], PHPExcel_Cell_DataType::TYPE_STRING);
						$page->getStyle('A'.($i + 2).':J'.($i + 2))->applyFromArray($style_wrap);
						$page->getStyle('A'.($i + 2).':J'.($i + 2))->getAlignment()->setWrapText(true);
					}					
				}
				
			$page->getColumnDimension('A')->setWidth(7);
			$page->getColumnDimension('B')->setWidth(18);
			$page->getColumnDimension('C')->setWidth(9.3);
			$page->getColumnDimension('D')->setWidth(15);
			$page->getColumnDimension('E')->setWidth(4.6);
			$page->getColumnDimension('F')->setWidth(12);
			$page->getColumnDimension('G')->setWidth(11);
			$page->getColumnDimension('H')->setWidth(4.7);
			$page->getColumnDimension('I')->setWidth(15);
			$page->getColumnDimension('J')->setWidth(25);
		  

		  $page->setTitle($_GET["year"]." ".$_GET["group_global"]); // Заголовок делаем 
		  /* Начинаем готовиться к записи информации в xlsx-файл */
		  $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
		  /* Записываем в файл */
		  $objWriter->save("exel.xlsx");
		
?>
<!DOCTYPE html>
<html>
<head>
	<title>Участники</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	<script src="js/regitration.js"></script>
</head>
<body>
	<a class="return_admin" href="edit.php">Назад</a>
	<div id="form_out">
		<h2>Форма вывода списка участников для всех желающих</h2>
		<hr />
		<table id="edit_form" border="0" cellspacing="0" cellpadding="0">
			<tr>
                <th>№ участника</th>
				<th>Группа</th>
				<th>ФИО</th>
				<th>Пол</th>
				<th>Регион</th>
				<th>Возраст (На 31 декабря пред. года)</th>
				<th>Спортивный разряд</th>
				<th>Выбранная дистанция</th>
				<th>Телефон</th>
				<th>E-mail</th>
				<th>Редактировать</th>
				<th>Удалить</th>
			<tr>
			<?php
				if($result === false) echo "Неизвестная ошибка! Попробуйте позже или обратитесь к администрации";
				else {
					for($i = 0; $i < count($result); $i++) {
						echo "<tr><td>".$result[$i]["number"]."</td><td>".$result[$i]["group"]."</td><td>".$result[$i]["fio"]."</td><td>".$result[$i]["sex"]."</td><td>".$result[$i]["region"]."</td><td>".$result[$i]["years"]."</td><td>".$result[$i]["razr"]."</td><td>".$result[$i]["dist"]."</td><td>".$result[$i]["tel"]."</td><td>".$result[$i]["email"]."</td><td><img onclick = 'editUser(".$result[$i]["id"].")' class='edit_user' id='edit_".$result[$i]["id"]."' src='img/pansel.png' alt='Ред.' /></td><td><img onclick = 'delUser(".$result[$i]["id"].")' class='del_user' id='del_".$result[$i]["id"]."' src='img/korz.png' alt='Удалить' /></td></tr>";	
					}					
				}
			?>
		</table>
	</div>
	<a class="button" href="print.php?year=<?=$_GET["year"]?>&group_global=<?=$_GET["group_global"]?>">Распечатать список</a>
	<a class="button" href="registration.php">Добавить участника</a>
	<a class="button" href="exel.xlsx">Сохранить в формате EXCEL</a>
	<span id="kol_uch">Всего участников: </span><span><?=count($result)?></span>
</body>