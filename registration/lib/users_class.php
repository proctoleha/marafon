<?php

require_once "global_class.php";

class Users extends GlobalClass
{

    public function __construct($db)
    {
        parent::__construct("users", $db);
    }

    public function addUser($data)
    {
        if (!$this->checkValid($data["number"], $data["fio"], $data["sex"], $data["tel"],
            $data["email"], $data["date"], $data["dist"], $data["region"], $data["razr"], $data["group"],
            $data["group_global"], $data["year"], $data["years"]))
            return false;
        return $this->add($data);
    }

    public function deleteUser($id)
    {
        return $this->delete($id);
    }

    public function editUser($data)
    {
        if (!$this->checkValid($data["number"], $data["fio"], $data["sex"], $data["tel"],
            $data["email"], $data["date"], $data["dist"], $data["region"], $data["razr"], $data["group"],
            $data["group_global"], $data["year"], $data["years"]))
            return false;
        $id = $data["id"];
        unset($data["id"]);
        unset($data["date"]);
        unset($data["group_global"]);
        unset($data["year"]);
        return $this->edit($id, $data);
    }

    public function isExistEmail($email)
    {
        return $this->isExist("email", $email);
    }

    public function isExistNumber($number, $year, $group_global)
    {
        $array = $this->getAllOnField("number", $number);
        if ($array === false)
            return false;
        for ($i = 0; $i < count($array); $i++)
        {
            if ($array[$i]["year"] != $year)
                continue;
            if ($array[$i]["group_global"] != $group_global)
                continue;
            return false;
        }
        return true;
    }

    public function getAllOnYear($year, $group_global)
    {
        return $this->getAllOnMoreField(array("year" => $year, "group_global" => $group_global),
            array("group", "fio"));
    }

    public function newNumber($year, $group_global)
    {
        $number = 1;
        $array = $this->getAllOnMoreField(array("year" => $year, "group_global" => $group_global),
            "number");
        for ($i = 0; $i < count($array); $i++)
        {
            if ($array[$i]["number"] != $i + 1)
            {
                $number = $i + 1;
                break;
            }
        }
        if (($number == 1) && ($i != 0))
            $number = $i + 1;
        return $number;
    }

    private function checkValid($number, $fio, $sex, $tel, $email, $date, $dist, $region,
        $razr, $group, $group_global, $year, $years)
    {
        if (!$this->valid->validNumber($number))
            return false;
        if (!$this->valid->validStringData($fio))
            return false;
        if (!$this->valid->validStringData($sex))
            return false;
        if (!$this->valid->validStringData($tel))
            return false;
        if (!$this->valid->validEmail($email))
            return false;
        if (!$this->valid->validDate($date))
            return false;
        if (!$this->valid->validStringDataSelect($dist))
            return false;
        if (!$this->valid->validStringDataSelect($region))
            return false;
        if (!$this->valid->validStringDataSelect($razr))
            return false;
        if (!$this->valid->validStringData($group))
            return false;
        if (!$this->valid->validStringData($group_global))
            return false;
        if (!$this->valid->validYear($year))
            return false;
        if (!$this->valid->validYears($years))
            return false;
        if (!$this->isExistNumber($number, $year, $group_global))
            return false;
        return true;
    }

}

?>