<?php

require_once "config_class.php";
require_once "checkvalid_class.php";

class DataBase
{

    private $valid;
    private $config;
    private $mysqli;

    public function __construct()
    {
        $this->valid = new CheckValid();
        $this->config = new Config();
        $this->mysqli = new mysqli($this->config->host, $this->config->user, $this->
            config->password, $this->config->db);
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    private function query($query)
    {
        return $this->mysqli->query($query);
    }

    private function select($table_name, $fields, $where = "", $order = "", $up = true,
        $limit = "")
    {
        for ($i = 0; i < count($filds); $i++)
        {
            if ((strpos($fields[$i], "(") === false) && ($fields[$i] != "*"))
                $fields[$i] = "`" . $fields[$i] . "`";
        }
        $fields = implode(",", $fields);
        $table_name = $this->config->db_prefix . $table_name;

        if (is_array($order))
        {
            foreach ($order as $k => $v) 
            {
                $order[$k] = "`$v`";
            }
            
            $order = "ORDER BY " . implode(',', $order);
        }
        else
        {
            if (!$order)
                $order = "ORDER BY `id`";
            elseif ($order != "RAND()")
            {
                $order = "ORDER BY $order";
            }
            else
                $order = "ORDER BY $order";
            if (!$up)
                $order .= " DESC";
        }


        if ($limit)
            $limit = "LIMIT $limit";
        if ($where)
            $query = "SELECT $fields FROM `$table_name` WHERE $where $order $limit";
        else
            $query = "SELECT $fields FROM `$table_name` $order $limit";
        
        $result_set = $this->query($query);
        if (!$result_set)
            return false;
        $i = 0;
        $data = array();
        while ($row = $result_set->fetch_assoc())
        {
            $data[$i] = $row;
            $i++;
        }
        $result_set->close;
        return $data;
    }

    public function insert($table_name, $nev_values)
    {
        $table_name = $this->config->db_prefix . $table_name;
        $query = "INSERT INTO `$table_name` (";
        foreach ($nev_values as $field => $value)
            $query .= "`$field`,";
        $query = substr($query, 0, -1);
        $query .= ") VALUES (";
        foreach ($nev_values as $value)
            $query .= "'" . addslashes($value) . "',";
        $query = substr($query, 0, -1);
        $query .= ")";
        return $this->query($query);
    }

    public function update($table_name, $ubd_fields, $where)
    {
        $table_name = $this->config->db_prefix . $table_name;
        $query = "UPDATE `$table_name` SET ";
        foreach ($ubd_fields as $field => $value)
            $query .= "`$field`='" . addslashes($value) . "',";
        $query = substr($query, 0, -1);
        if ($where)
        {
            $query .= " WHERE $where";
            //echo $query;
            return $this->query($query);
        }
        return false;
    }

    public function delete($table_name, $where)
    {
        $table_name = $this->config->db_prefix . $table_name;
        if ($where)
        {
            $query = "DELETE FROM `$table_name` WHERE $where";
            //echo $query;
            return $this->query($query);
        }
        return false;
    }

    public function deleteAll($table_name)
    {
        $table_name = $this->config->db_prefix . $table_name;
        $query = "TRUNCATE TABLE `$table_name`";
        return $this->query($query);
    }

    public function getField($table_name, $field_out, $field_in, $value_in)
    {
        $data = $this->select($table_name, array($field_out), "`$field_in`='" .
            addslashes($value_in) . "'");
        if (count($data) == 1)
            return $data[0][$field_out];
        return false;
    }

    public function getElementOnField($table_name, $field_in, $value_in)
    {
        $data = $this->select($table_name, array("*"), "`$field_in`='" . addslashes($value_in) .
            "'");
        if (count($data) == 1)
            return $data[0];
        return false;
    }

    public function getFieldOnID($table_name, $id, $field_out)
    {
        if ($this->existID($table_name, $id))
            return $this->getField($table_name, $field_out, "id", $id);
        return false;
    }

    private function existID($table_name, $id)
    {
        if (!$this->valid->validID($id))
            return false;
        $data = $this->select($table_name, array("id"), "`id`='" . addslashes($id) . "'");
        if (!$data)
            return false;
        return true;
    }

    public function getAll($table_name, $order = "", $up = true)
    {
        return $this->select($table_name, array("*"), "", $order, $up);
    }

    public function deleteOnID($table_name, $id)
    {
        if (!$this->existID($table_name, $id))
            return false;
        return $this->delete($table_name, "`id`='$id'");
    }

    public function setField($table_name, $field, $value, $field_in, $value_in)
    {
        return $this->update($table_name, array($field => addslashes($value)), "`$field_in`='" .
            addslashes($value_in) . "'");
    }

    public function setFieldOnID($table_name, $id, $field, $value)
    {
        if (!$this->existID($table_name, $id))
            return false;
        return $this->setField($table_name, $field, $value, "id", $id);
    }

    public function getAllOnField($table_name, $field, $value, $order = "", $up = true)
    {
        return $this->select($table_name, array("*"), "`$field`='" . addslashes($value) .
            "'", $order, $up);
    }

    public function getElementOnID($table_name, $id)
    {
        if (!$this->existID($table_name, $id))
            return false;
        $data = $this->select($table_name, array("*"), "`id`='$id'");
        return $data[0];
    }

    public function getRandomElement($table_name, $count)
    {
        return $this->select($table_name, array("*"), "", "RAND()", true, $count);
    }

    public function getCount($table_name)
    {
        $data = $this->select($table_name, array("COUNT(`id`)"));
        return $data[0]["COUNT(`id`)"];
    }

    public function isExist($table_name, $field, $value)
    {
        $data = $this->select($table_name, array("id"), "`$field`='" . addslashes($value) .
            "'");
        if (count($data) == 0)
            return false;
        return true;
    }

    public function maxID($table_name)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("MAX(`id`)"));
        return $data[0]["MAX(`id`)"];
    }

    public function maxElementOnField($table_name, $field)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("MAX(`$field`)"));
        return $data[0]["MAX(`$field`)"];
    }

    public function minID($table_name)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("MIN(`id`)"));
        return $data[0]["MIN(`id`)"];
    }

    public function minElementOnField($table_name, $field)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("$field"), "", "", true, 1);
        return $data[0]["$field"];
    }

    public function getElementOnInterval($table_name, $field, $from, $to)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("*"), "", "$field", true);
        if (!$data)
            return false;
        $array = array();
        $j = 0;
        for ($i = 0; $i < count($data); $i++)
        {
            if (($data[$i][$field] >= $from) && ($data[$i][$field] <= $to))
            {
                $array[$j] = $data[$i];
                $j++;
            }
        }
        if (count($array))
            return $array;
        return false;
    }

    public function countElementOnInterval($table_name, $field, $from, $to)
    {
        if (!$this->getCount($table_name))
            return false;
        $data = $this->select($table_name, array("*"), "", "$field", true);
        if (!$data)
            return false;
        $array = array();
        $j = 0;
        for ($i = 0; $i < count($data); $i++)
        {
            if (($data[$i][$field] >= $from) && ($data[$i][$field] <= $to))
            {
                $array[$j] = $data[$i];
                $j++;
            }
        }
        if (count($array))
            return count($array);
        return false;
    }

    public function search($table_name, $words, $field)
    {
        $words = strtolower($words);
        $words = trim($words);
        $words = quotemeta($words);
        if ($words == "")
            return false;
        $where = "";
        $arraywords = explode(" ", $words);
        $logic = "OR";
        foreach ($arraywords as $key => $value)
        {
            if (isset($arraywords[$key - 1]))
                $where .= $logic;
            for ($i = 0; $i < count($field); $i++)
            {
                $where .= "`" . $field[$i] . "` LIKE '%" . addslashes($value) . "%'";
                if (($i + 1) != count($field))
                    $where .= " OR";
            }
        }
        $result = $this->select($table_name, array("*"), $where);
        return $result;
    }

    public function getAllOnMoreField($table_name, $more_field, $order = "", $up = true)
    {
        $where = "";
        foreach ($more_field as $key => $value)
        {
            $where .= "`$key`='" . addslashes($value) . "' AND ";
        }
        $where = substr($where, 0, -5);
        return $this->select($table_name, array("*"), $where, $order, $up);
    }

    public function __destruct()
    {
        if ($this->mysqli)
            $this->mysqli->close();
    }
}

?>