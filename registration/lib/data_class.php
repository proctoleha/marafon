<?php 
require_once "global_class.php";

class Data extends GlobalClass {
	
	public function __construct($db) {
		parent::__construct("data", $db);
	}
	
	public function addData($data) {
		if($this->valid->isContainQuotesArray($data)) return false;
		return $this->add($data);
	}
	
	public function editData($ind, $data) {
		if($this->valid->isContainQuotes($data)) return false;
		$id = $this->getField("id", "ind", $ind);
		if($id === false) return false;
		$data = array("ind" => $ind, "data" => $data);
		return $this->edit($id, $data);
	}
	
	public function getData($value){
		return $this->getField("data", "ind", $value);
	}
	
	
}
?>