<?php 
require_once "global_class.php";

class Article extends GlobalClass {
	
	public function __construct($db) {
		parent::__construct("article", $db);
	}
	
	public function getAllSortDate(){
		return $this->getAll("date", false);
	}
	
	public function getAllOnMenu($menu){
		return $this->getAllOnField("menu", $menu, "date", false);
	}
	
	public function getTitle($id) {
		return $this->getFieldOnID($id, "title");
	}
	
	public function getMenu($id) {
		return $this->getFieldOnID($id, "menu");
	}
	
	public function getImage($id) {
		return $this->getFieldOnID($id, "image");
	}
	
	public function getSubImage($id) {
		return $this->getFieldOnID($id, "sub_image");
	}
	
	public function getDescription($id) {
		return $this->getFieldOnID($id, "description");
	}
	
	public function getArticle($id) {
		return $this->getFieldOnID($id, "article");
	}
	
	public function getDate($id) {
		return $this->getFieldOnID($id, "date");
	}
	
	public function getLink($id) {
		return $this->getFieldOnID($id, "link");
	}
	
	public function addArticle($upd_fields) {
		return $this->add($upd_fields);
	}
	
	public function set($id, $upd_fields) {
		return $this->edit($id, $upd_fields);
	}
	
	public function setTitle($id, $value) {
		return $this->setFieldOnID($id, "title", $value);
	}
	
	public function setMenu($id, $value) {
		return $this->setFieldOnID($id, "menu", $value);
	}
	
	public function setDescription($id, $value) {
		return $this->setFieldOnID($id, "description", $value);
	}
	
	public function setArticle($id, $value) {
		return $this->setFieldOnID($id, "article", $value);
	}
	
	public function setDate($id, $value) {
		return $this->setFieldOnID($id, "date", $value);
	}
	
	public function setLink($id, $value) {
		return $this->setFieldOnID($id, "link", $value);
	}
	
	public function search($words) {
		return $result = $this->searchWords($words, array("title", "description", "article"));
	}
	
}
?>