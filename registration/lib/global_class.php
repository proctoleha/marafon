<?php 
require_once "config_class.php";
require_once "checkvalid_class.php";
require_once "database_class.php";
	
abstract class GlobalClass {
		
		protected $db;
		protected $valid;
		protected $config;
		protected $table_name;
	
	protected function __construct($table_name, $db) {
		$this->db = $db;
		$this->table_name = $table_name;
		$this->valid = new CheckValid();
		$this->config = new Config();
	}
	
	protected function add($new_values) {
		return $this->db->insert($this->table_name, $new_values);
	}
	
	protected function edit($id, $upd_fields) {
		return $this->db->update($this->table_name, $upd_fields, "`id`='$id'");
	}
		
	public function delete($id) {
		return $this->db->delete($this->table_name, "`id`='$id'");
	}
	
	public function deleteAll() {
		return $this->db->deleteAll($this->table_name);
	}
		
	protected function getField($field_out, $field_in, $value_in) {
		return $this->db->getField($this->table_name, $field_out, $field_in, $value_in);
	}
	
	public function getElementOnField($field, $value) {
		return $this->db->getElementOnField($this->table_name, $field, $value);
	}
	
	protected function getFieldOnID($id, $field) {
		return $this->db->getFieldOnID($this->table_name, $id, $field);
	}
	
	protected function setFieldOnID($id, $field, $value) {
		return $this->db->setFieldOnID($this->table_name, $id, $field, $value);
	}
	
	protected function getIDOnField($field, $value) {
		return $this->getField("id", $field, $value);
	}
	
	public function get($id) {
		return $this->db->getElementOnID($this->table_name, $id);
	}
	
	public function getAll($order = "", $up = true) {
		return $this->db->getAll($this->table_name, $order, $up);
	}
	
	public function getAllOnField($field, $value, $order = "", $up = true) {
		return $this->db->getAllOnField($this->table_name, $field, $value, $order, $up);
	}
	
	public function getRandomElement($count) {
		return $this->db->getRandomElement($this->table_name, $count);
	}

	public function getLastID() {
		return $this->db->maxID($this->table_name);
	}
	
	protected function isExist($field, $value) {
		return $this->db->isExist($this->table_name, $field, $value);
	}
	
	public function getElementOnInterval($field, $from, $to) {
		return $this->db->getElementOnInterval($this->table_name, $field, $from, $to);
	}
	
	public function countElementOnInterval($field, $from, $to) {
		return $this->db->countElementOnInterval($this->table_name, $field, $from, $to);
	}
	
	public function searchWords($words, $field) {
		return $this->db->search($this->table_name, $words, $field);
	}
	
	public function getAllOnMoreField($more_field, $order = "", $up = true) {
		return $this->db->getAllOnMoreField($this->table_name, $more_field, $order, $up);
	}
}
?>