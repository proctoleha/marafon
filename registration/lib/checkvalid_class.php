<?php 
	require_once "config_class.php";
	
	class CheckValid {
		
		private $config;
		
		public function __construct() {
			$this->config = new Config();
		}
		
		public function validID($id) {
			if(!$this->isIntNumber($id)) return false;
			if($id <= 0) return false;
			return true;
		}
		
		private function isIntNumber($number){
			if((!is_int($number)) && (!is_string($number))) return false;
			if(!preg_match("/^((-?[1-9][0-9]*)|(0))$/", $number)) return false;
			return true;
		}
		
		private function isNoNegativeNumber($number) {
			if(!$this->isIntNumber($number)) return false;
			if($number < 0) return false;
			return true;	
		}
		
		public function isContainQuotes($string) {
			$array = array("\"", "'", "`", "&quot;", "&apos;");
			foreach($array as $field => $value){
				if(strpos($string, $value) !== false) return true;
			}
			return false;
		}
		
		private function isOnlyLettersAndDigits($string) {
			if((!is_int($string)) && (!is_string($string))) return false;
			if(!preg_match("/[a-zа-я0-9]*/i", $string)) return false;
			return true;
		}
		
		private function validString($string, $min_length, $max_length) {
			if(!is_string($string)) return false;
			if(strlen($string) > $max_length) return false;
			if(strlen($string) < $min_length) return false;
			return true;
		}
		
		public function validEmail($email) {
			if(!$this->validString($email, 6, 255));
			$reg = "/^([a-z0-9\-_\.]*)*[a-z0-9]@([a-z0-9\-\.]*)*[a-z0-9]\.[a-z]+$/i";
			return preg_match($reg, $email);
			
		}
		
		public function validStringData($string) {
			if($this->isContainQuotes($string)) return false;
			return $this->validString($string, 1, 255);
		}
		
		public function validStringDataSelect($string) {
			if($this->isContainQuotes($string)) return false;
			if($string == "null") return false;
			return $this->validString($string, 1, 255);
		}

		public function validDate($date) {
			return $this->isIntNumber($date);
		}
		
		public function validYear($date) {
			return $this->isNoNegativeNumber($date);
		}
		
		public function validYears($date) {
			if(!$this->isNoNegativeNumber($date)) return false;
			if($date > 9999) return false;
			return true;
		}
		
		public function validNumber($number) {
			if((!is_int($number)) && (!is_string($number))) return false;
			if(!preg_match("/^(([1-9][0-9]*)|(0))$/", $number)) return false;
			return true;
		}
		
		public function isContainQuotesArray($data) {
			for($i = 0; $i < count($data); $i++){
			if($this->isContainQuotes($data[$i])) return true;
			}
			return false;
		}
		
	}
?>