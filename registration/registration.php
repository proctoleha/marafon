<!DOCTYPE html>
<html>
<head>
	<title>Регистрация</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	<script src="js/regitration.js"></script>
	<script src="js/calendar_kdg.js" type="text/javascript"></script>
</head>
<body>
	<div id="reg_form">
		<h2>Форма для заполнения участником весеннего марафона</h2>
		<hr />
		<h3>Уведомляем, присвоение группы определяется количеством полных лет участника марафона на 31 декабря настоящего года</h3><br />
		<form id="reg_data_test" action="#" method="post" onsubmit="return chekDate()">
			<div class="reg_form">
				<input type="text" name="famaly" required="required" pattern="[^]{3,}" placeholder="Фамилия" /><br />
				<input type="text" name="name" required="required" pattern="[^]{3,}"  placeholder="Имя" /><br />
				<input type="text" name="ochestvo" required="required" pattern="[^]{3,}"  placeholder="Очество" /><br />
				<input class="radio" checked style="display: none" type="radio" name="sex" value="null" />
				<input class="radio" type="radio" name="sex" value="Мужчина" />Мужчина
				<input class="radio" type="radio" name="sex" value="Женщина" />Женщина<br />
				<input type="text" name="tel"  required="required" pattern="[^]{3,}" placeholder="Введите Ваш телефон" /><br />
				<input type="text" name="email" required="required" placeholder="Введите Ваш E-mail" /><br />
			</div>
			<div class="reg_form">
				<input id="select_date" type="text" name="date" required="required" placeholder="Дата рождения дд-мм-гггг" onfocus="this.select();_Calendar.lcs(this)" onclick="event.cancelBubble=true;this.select();_Calendar.lcs(this)" /><br />
				<select id="region" name="region">
					<option value = "null">Выберете регион</option>
					<?php 	$array = explode("\n", file_get_contents("regions.txt"));
							for($i = 0; $i < count($array); $i++) {
								echo "<option>".$array[$i]."</option>";
							}?>
				</select><br />
				<select id="razr" name="sport_razr">
					<option value = "null">Спортивный разряд</option>
					<option>без разряда</option>
					<option>3 разряд</option>
					<option>2 разряд</option>
					<option>1 разряд</option>
					<option>КМС (кандидат в мастера спорта)</option>
					<option>МС (мастер спорта)</option>
					<option>МСМК (мастер спорта международного класса)</option>
					<option>ЗМС (заслуженный мастер спорта)</option>
				</select><br />
				<select id="dist" name="dist">
					<option value = "null">Выберете дистанцию</option>
				</select><br />
				<img  id="kapcha" src="kapcha.php" alt="kapcha" />
				<input type="text" name="kapcha" required="required" pattern="[0-9]{4}" placeholder="Введите число с картинки" /><br />
				
			</div>
			<div class="clear"></div>
			<div class="red">Внимание! Внимательно проверьте всю введенную информацию. Изменить ее будет невозможно.</div>
			<div>
				<input type="submit" name="submit_reg" value="Отправить мои данные" />
			</div>
		</form>
	</div>
</body>