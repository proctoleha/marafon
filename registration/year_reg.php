<?php
	session_start();
	require_once "lib/config_class.php";
	require_once "lib/data_class.php";
	require_once "lib/checkvalid_class.php";
	
	$valid = new CheckValid();
	$config = new Config();
	$db = new DataBaseReg();
	$data =  new Data($db);
	
	if(($_SESSION["login"] !== $config->admin_name) && ($_SESSION["pass"] !== $config->admin_pass)) {
		Header ("Location: index.php");
		exit;
	}
	
	if(isset($_POST["year_reg"])){
		$year = $_POST["year"];
		if(!$valid->isContainQuotes($year)){
			if($data->editData("year", $year)) $result = "success";
			else $result = "error";
		}
	}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Марафон</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/regitration.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	<script src="js/regitration.js"></script>
</head>
<body>
<br />
<br />
<a class="return_admin" href="edit.php">Назад</a>
<div id="admin">
	<?php 
		if(isset($result)){
			if($result == "success") echo "<p>Изменение года выполнено</p>";
			else echo "<p>Изменение года не удалось</p>";
		}
		?>
	<p>Год проведения соревнований: <?=$data->getData("year")?></p>
	<p>Определить новый год проведения соревнований</p>
	<hr />
	<form action="year_reg.php" method="post" onsubmit="return yearSuc()">
		<table>
			<tr>
				<td>Выберите год:</td>
				<td>
					<select name="year">
						<?php
							$year = date("Y", time());
							for($i = 0; $i < 10; $i++){
								echo "<option>".($year - 1 + $i)."</option>";
							}
						?>
					
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="year_reg" value="Определить" />
				</td>
			</tr>
		</div>
	</form>
</div>
</body>