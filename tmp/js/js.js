jQuery.fn.center = function () {

    var w = jQuery(window);

    this.css("display", "block");

    this.css("position", "absolute");

    this.css("z-index", "11000");

    this.css("top", w.scrollTop() + 20 + "px");

    this.css("left", (w.width() - this.width()) / 2 + w.scrollLeft() + "px");

    return this;

};



function checkEmail (mail) {

    return (/[0-9a-z_]+@[0-9a-z_^.]+\.[a-z]{2,3}/i).exec(mail) !== null ? true : false;

}



function str_replace(search, replace, subject) {

    return subject.split(search).join(replace);

}

function trim (str) {

    return str.replace(/(^\s+)|(\s+jQuery)/g, "");

}

function show_form_msg (url, obj, f) {

    jQuery.post(url, obj, function (data) {

        if (f !== undefined) {

            if (data === 0){

                alert('Такой участник на этот год уже существует');

                return false;

            } else if (data === -1) {

                alert('Произошла неизвестная ошибка. Обратитесь к админу.');

                return false;

            }
        }

        let modal_prod_img = jQuery('.modal_prod_img');

        modal_prod_img.html("<img alt='close' class='close__' src=/tmp/i/close_small.png />" + data);

        modal_prod_img.leanModal();

    });

}



jQuery.fn.leanModal = function () {
    jQuery("#lean_overlay").fadeTo(200, .9);
    this.center().fadeTo(500, 1);
};


jQuery('body').on('click', '#lean_overlay, .close__', function () {
    location.reload();
});


var data_processing = 'http://' + window.location.hostname + '/reg/data_processing.php';
jQuery(function () {
    (function () {
        jQuery.post(data_processing, {name_f : 'count_reg'}, function (data) {
            var obj = jQuery.parseJSON(data);
            jQuery('#cur_year').text(obj['year']);
            jQuery('#child').text(obj['child']);
            jQuery('#adults').text(obj['adults']);

        });

        jQuery.post(data_processing, {name_f : 'echo_get_cur_year'}, function (data) {
            let get_g = jQuery('.get_g');
            if (get_g.attr('data-y') === '')
                get_g.attr('data-y', data);
        });

        jQuery.post(data_processing, {name_f : 'check_dates'}, function (data) {
            if (data != 1) {
                jQuery('#reg_form').html(data);
            }

        });
    }());

    jQuery('#slider').rhinoslider({

        effect: 'turnOver',

        easing: 'easeInOutQuad',

        controlsPrevNext: false,

        controlsPlayPause: false,

        animateActive: true,

        slidePrevDirection: 'toRight',

        slideNextDirection: 'toRight',

        showTime: 10000,

        autoPlay: true

    });

    jQuery('.fancy').fancybox();

    jQuery('#easypollform div input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // не обязательно
    });

    jQuery('#news_on_main').click(function () {
        window.location.href = jQuery('#news_on_main a').attr('href');
    });



    jQuery('.get_g').click(function (e) {
        e.preventDefault();
        var obj = {
            year: jQuery(this).attr('data-y'),
            cat: jQuery(this).attr('data-age'),
            name_f : 'get_group'

        };
        show_form_msg(data_processing, obj);
    });

});