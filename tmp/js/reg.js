jQuery(function () {
	
    jQuery('#reg_data input[name=phone]').inputmask("8(999)999-99-99");
    jQuery('#reg_data input[name=date_birth]').inputmask("99.99.9999"); 

    function check_date_birth () {
        var date_birth = (jQuery('input[name=date_birth]').val()).split('.');
        if (parseInt(date_birth[0]) > 0 && parseInt(date_birth[0]) <= 31 && 
            parseInt(date_birth[1]) > 0 && parseInt(date_birth[1]) <= 12 &&
            parseInt(date_birth[2]) > 1900
        ) return true;
        return false;       
    }
    
    function create_length () {
        var bool = true;
        if (!jQuery("input[name=sex]:checked").length) {
            alert('Выберите пол, прежде чем идти дальше');
            bool = false;
        }
        if (check_date_birth()) {
            date_birth = jQuery('input[name=date_birth]').val();
        } else {
            alert("Некорректная дата рождения");
            bool = false;
        }
        if (bool) {
            var obj = {
                name_f : 'get_data_length_group',
                date_birth : date_birth,
                sex : jQuery("input[name=sex]:checked").val()            
            };
            jQuery.post(data_processing, obj, function (data) {
                obj = jQuery.parseJSON(data);
                console.log( obj );
                if (obj.year === undefined) {
                    jQuery('.dist0').prop({'checked' : true, 'disabled' : false}).val(obj[0]['id']).next('span').text(obj[0]['length'] + ' км.');
                    if (obj.length > 1) { 
                       jQuery('.dist1').prop('disabled', false).val(obj[1]['id']).next('span').text(obj[1]['length'] + ' км.'); 
                    } else {
                        jQuery('.dist1').val('').prop('disabled', true).next('span').text('');
                    }                    
                } else {
                    alert('Проверьте дату рождения! Вам или меньше 6-ти лет, или больше 18-ти. В ' + obj.year + ' году проводится только детский марафон!');
                }
            });
        }
    }

    jQuery("input[name=sex]").change(function () {
        if (check_date_birth()) {
            create_length ();
        }
    });
    
    jQuery('input[name=date_birth]').blur(function () {
       create_length ();
    });
 
    jQuery('#reg_data').submit(function () {
        var bool = true;
        if (jQuery("input[name=id_g]").val()){
            // if (!checkEmail(jQuery('input[name=email]').val())) {
            //     alert ('Неверный email!');
            //     bool = false;
            // }
            if (jQuery('select[name=sports_category]').val() == null) {
                alert ('Выберите разряд!');
                bool = false;                
            }
            if (jQuery('select[name=region]').val() == '') {
                alert ('Выберите регион!');
                bool = false;                
            }            
            var phone = jQuery('input[name=phone]').val();
            if (!(/\([0-9]{3}\)[0-9]{3}\-[0-9]{2}\-[0-9]{2}/).test(phone)) {
                alert ('Некорректный номер телефона!');
                bool = false;                 
            }       
        } else {
            alert ('Нет данных о дистанции. Выберите корректно пол и д/р!');
            bool = false;
        } 

        if (bool) {                                                
            show_form_msg(data_processing, jQuery(this).serialize(), true); 
        }
        return false;
    });  
})    ;
