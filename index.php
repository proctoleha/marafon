<?php
require_once __DIR__.'/vendor/autoload.php';

use Dotenv\Environment\DotenvFactory;
use Dotenv\Environment\Adapter\EnvConstAdapter as Adapter;
use Dotenv\Dotenv;

(Dotenv::create(__DIR__, null, new DotenvFactory([new Adapter()])))->load();

if(!isset($_SERVER['REQUEST_TIME_FLOAT'])) $_SERVER['REQUEST_TIME_FLOAT'] = microtime(true);

$base_path = str_replace('\\','/',dirname(__FILE__)) . '/';
if(is_file($base_path . 'assets/cache/siteManager.php'))
    include_once($base_path . 'assets/cache/siteManager.php');
if(!defined('MGR_DIR') && is_dir("{$base_path}manager"))
	define('MGR_DIR', 'manager');
if(is_file($base_path . 'assets/cache/siteHostnames.php'))
    include_once($base_path . 'assets/cache/siteHostnames.php');
if(!defined('MODX_SITE_HOSTNAMES'))
	define('MODX_SITE_HOSTNAMES', '');

// get start time
$mstart = memory_get_usage();

// harden it
require_once(dirname(__FILE__).'/'.MGR_DIR.'/includes/protect.inc.php');

// set some settings, and address some IE issues
@ini_set('url_rewriter.tags', '');
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_only_cookies',1);
session_cache_limiter('');
header('P3P: CP="NOI NID ADMa OUR IND UNI COM NAV"'); // header for weird cookie stuff. Blame IE.
header('Cache-Control: private, must-revalidate');
ob_start();

/**
 *	Filename: index.php
 *	Function: This file loads and executes the parser. *
 */

define('IN_PARSER_MODE', true);
if ( ! defined('IN_MANAGER_MODE')) {
	define('IN_MANAGER_MODE', false);
}
if (!defined('MODX_API_MODE')) {
    define('MODX_API_MODE', false);
}

// get the required includes
if(!isset($database_user) || $database_user=="") {
	$rt = @include_once(dirname(__FILE__).'/'.MGR_DIR.'/includes/config.inc.php');
	// Be sure config.inc.php is there and that it contains some important values
	if(!$rt || !$database_type || !$database_server || !$database_user || !$dbase) {
		readfile('install/not_installed.tpl');
		exit;
	}
}

// start session
startCMSSession();

// initiate a new document parser
if (isset($coreClass) && class_exists($coreClass)) {
	$modx = new $coreClass;
}
if (!isset($modx) || !($modx instanceof \DocumentParser)) {
	include_once(MODX_MANAGER_PATH.'includes/document.parser.class.inc.php');
	$modx = DocumentParser::getInstance();
}

// set some parser options
$modx->minParserPasses = 1; // min number of parser recursive loops or passes
$modx->maxParserPasses = 10; // max number of parser recursive loops or passes
$modx->dumpSQL = false;
$modx->dumpSnippets = false; // feed the parser the execution start time
$modx->dumpPlugins = false;
$modx->tstart = $_SERVER['REQUEST_TIME_FLOAT'];
$modx->mstart = $mstart;

// Debugging mode:
$modx->stopOnNotice = false;

// Don't show PHP errors to the public
if(!isset($_SESSION['mgrValidated']) || !$_SESSION['mgrValidated']) {
    @ini_set("display_errors","0");
}

if(MODX_CLI){
    @set_time_limit(0);
    @ini_set('max_execution_time',0);
}

// execute the parser if index.php was not included
if (!MODX_API_MODE && !MODX_CLI) {
    $modx->executeParser();
}
