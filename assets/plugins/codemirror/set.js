CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: false,
		matchBrackets: false,
		mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift"       
   });
