<?php
/**
 * ------------------------------------------------------------------------------
 * English Language File for EasyPoll Module. Keep this File UTF-8 encoded
 * ------------------------------------------------------------------------------
 * @author banal
 * @version 0.2 <2008-02-08>
 */

// ---- Formatting
$_lang['EP_date'] = '%Y-%m-%d %H:%i';

// ---- About screen
$_lang['EP_module_title'] = 'EasyPoll';
$_lang['EP_welcome_title'] = 'Добро пожаловать в EasyPoll Manager';
$_lang['EP_welcome_text']
= '<h1>EasyPoll Manager</h1>
<p>Добро пожаловать в EasyPoll Manager, управление голосованием (опросом).</p>';
$_lang['EP_info_version'] = 'Версия: <strong>%s</strong><br/>Автор: <strong>Roman Schmid, AKA banal</strong>';
$_lang['EP_not_installed'] = 'EasyPoll Manager еще не установлен.';
$_lang['EP_installbutton'] = 'Нажмите для установки';

// ---- install screen
$_lang['EP_install_title'] = 'Инсталляция EasyPoll Manager';
$_lang['EP_installsuccess']
= '<h1>Инсталляция выполнена</h1>
<p>EasyPoll Manager успешно установлен. Можете удалить файл <strong>setup.sql</strong> с Вашего сервера. Путь к файлу: %s</p>';

// ---- Button Titles
$_lang['EP_back'] = 'Назад';
$_lang['EP_edit'] = 'Редактировать';
$_lang['EP_delete'] = 'Удалить';
$_lang['EP_create'] = 'Создать';
$_lang['EP_save'] = 'Сохранить';
$_lang['EP_cancel'] = 'Отмена';
$_lang['EP_confirmdelete'] = 'Подвердить удаление';
$_lang['EP_editchoice'] = 'Редактировать ответы';
$_lang['EP_stop_editchoice'] = 'Остановить редактирование выбранного';

// ---- General stuff
$_lang['EP_title_create'] = 'Создать новую запись';


// ---- Tab Titles
$_lang['EP_tab_about'] = 'О модуле';
$_lang['EP_tab_language'] = 'Языки';
$_lang['EP_tab_polls'] = 'Опросы';
$_lang['EP_tab_admin'] = 'Административные задачи';

// ---- Language Screen
$_lang['EP_lang_title'] = 'Языки EasyPoll Management';
$_lang['EP_lang_text']
= 'Добавьте, удалите или измените языки, для которых Вы хотите создавать Опросы. <br/>Пожалуйста, отметьте, что Вы должны проставлять все Вопросы
и ответы на каждом созданном языке.';
$_lang['EP_lang_short'] = 'Код языка (1-3 символа)';
$_lang['EP_lang_long'] = 'Название языка';
$_lang['EP_lang_del']
= 'Вы действительно хотите удалить язык \'%s\'? <strong>Пожалуйста, отметьте, что вы удалите все вопросы и ответы связанные с данным языком!</strong>';

// ---- Polls and Choices Screen
$_lang['EP_polls_title'] = 'Вопросы EasyPol Management';
$_lang['EP_polls_text']
= 'Добавьте, удалите или измените Опросы<br/><strong>T</strong> = Статус перевода (переворачивают символ, чтобы видеть детали). Опросы, которые не полностью переведены, считают бездействующими<br/>
<strong>A</strong> = Действительно ли Jghjc является активным/видимым? Бездействующие Опросы никогда не видимы на веб-сайте, независимо от того во что Вы входите в качестве Начала - или Дата окончания.<br/>
<strong>V</strong> = Число голосов за этот опрос<br/>
<strong>C</strong> = Число Выбора для этого Опроса. Опросы, у которых есть нулевой выбор, считают бездействующими';

$_lang['EP_poll_title'] = 'Внутреннее Название';
$_lang['EP_poll_sdate'] = 'Дата начала';
$_lang['EP_poll_edate'] = 'Дата окончания';
$_lang['EP_poll_active'] = 'Видимость';
$_lang['EP_poll_transl'] = 'Перевод';
$_lang['EP_poll_transl_short'] = '<span title="Translation status">T</span>';
$_lang['EP_poll_active_short'] = '<span title="опрос видимый?">A</span>';
$_lang['EP_poll_votes_short'] = '<span title="Число вопросов">V</span>';
$_lang['EP_poll_choices_short'] = '<span title="Число ответов">C</span>';
$_lang['EP_poll_votes'] = 'Вопрос';
$_lang['EP_poll_new'] = 'Создать новый опрос';
$_lang['EP_poll_edit'] = 'Редактировать опрос';
$_lang['EP_poll_del']
= 'Do you really want to delete the Poll \'%s\'? <strong>Please note, that this action will delete all Choices, Votes and Translations
associated with this Poll</strong>';

$_lang['EP_transl_complete'] = 'Перевод выполнен';
$_lang['EP_transl_missing'] = '%d items to be translated';

$_lang['EP_choices_title'] = 'Poll choices';
$_lang['EP_choice_new'] = 'Создать новый выбор';
$_lang['EP_choice_edit'] = 'Редактировать выбор';
$_lang['EP_choice_del']
= 'Do you really want to delete the Choice \'%s\'? <strong>Please note, that this action will delete all Translations and Votes
associated with this Choice</strong>';

// ---- Admin Screen
$_lang['EP_admin_title'] = 'Административные задачи EasyPoll';
$_lang['EP_admin_text']
= 'Perform administrative tasks regarding your Polls.<br/><strong>Please note that these actions won\'t ask for confirmation.
They will be executed immediately. So think before you click</strong>';
$_lang['EP_clear_ip'] = 'Clear all logged user IPs';
$_lang['EP_clear_success'] = 'The logged IP addresses have been deleted';

// ---- ERRORS AND WARNINGS

$_lang['EP_error_title'] = 'An error occured';
$_lang['EP_warning_title'] = 'Внимание';
// %s placeholder will be replaced with file path
$_lang['EP_sqlfile_warn']
= 'The <strong>setup.sql</strong> file still exists. For safety reasons you should delete
it from the server. <br/><strong>Path:</strong> %s';

$_lang['EP_sqlfile_error']
= '<h1>Setup failed</h1>
<p>The <strong>setup.sql</strong> file does not exist or is not readable. This file is required
in order to install the DB Tables. Please make sure you place the file in the modules/EasyPoll/
folder and make sure it is readable by PHP. If that\'s not possible (in case of php safe mode) you
should create the tables manually by running the SQL commands from the file in phpMyAdmin or another
Database Administration Tool.</p>';

$_lang['EP_sqlcreate_error']
= '<h1>Setup failed</h1>
<p>Creation of the required DB Tables failed. Please note, EasyPoll requires <strong>MySQL Version 4.1</strong> or higher.
If your DB meets this requirement and you still get this error, please consider adding the Tables 
manually by running the create commands from the setup.sql file on your DB. Use phpMyAdmin or a similar tool for that task.</p>';

// %s placeholder will be replaced with file path
$_lang['EP_noclassfile'] = 'Fatal Error: Class-File does not exist: %s';

$_lang['EP_ex_undef'] = 'Unspecified EasyPoll Exception: %s';
$_lang['EP_db_error'] = 'Database error. A SQL Command failed';
$_lang['EP_ex_invalidparam'] = 'Invalid Parameter supplied for: \'%s\'';
?>