#
# ВМ Database Dump
# MODX Version:1.2.1
# 
# Host: 
# Generation Time: 19-12-2019 10:28:57
# Server version: 5.6.41-84.1
# PHP Version: 5.6.40
# Database: `stroyzas_vesenni`
# Description: 
#

# --------------------------------------------------------

#
# Table structure for table `modx_active_user_locks`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_user_locks`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_user_locks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `elementType` int(1) NOT NULL DEFAULT '0',
  `elementId` int(10) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_element_id` (`elementType`,`elementId`,`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=265 DEFAULT CHARSET=latin1 COMMENT='Contains data about locked elements.';

#
# Dumping data for table `modx_active_user_locks`
#


# --------------------------------------------------------

#
# Table structure for table `modx_active_user_sessions`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_user_sessions`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_user_sessions` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data about valid user sessions.';

#
# Dumping data for table `modx_active_user_sessions`
#

INSERT INTO `modx_active_user_sessions` VALUES ('630930916477994b0922c1ef188b4a12','1','1576740535','185.7.87.12, 185.7.87.12');


# --------------------------------------------------------

#
# Table structure for table `modx_active_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_users` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `action` varchar(10) NOT NULL DEFAULT '',
  `id` int(10) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data about last user action.';

#
# Dumping data for table `modx_active_users`
#

INSERT INTO `modx_active_users` VALUES ('630930916477994b0922c1ef188b4a12','1','admin','1576740535','93',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=cp1251 COMMENT='Categories to be used snippets,tv,chunks, etc';

#
# Dumping data for table `modx_categories`
#

INSERT INTO `modx_categories` VALUES ('1','Demo Content');

INSERT INTO `modx_categories` VALUES ('2','Login');

INSERT INTO `modx_categories` VALUES ('3','Manager and Admin');

INSERT INTO `modx_categories` VALUES ('4','Search');

INSERT INTO `modx_categories` VALUES ('5','Navigation');

INSERT INTO `modx_categories` VALUES ('6','Content');

INSERT INTO `modx_categories` VALUES ('7','Forms');

INSERT INTO `modx_categories` VALUES ('8','Template');

INSERT INTO `modx_categories` VALUES ('9','Изменяемые данные');

INSERT INTO `modx_categories` VALUES ('10','System');

INSERT INTO `modx_categories` VALUES ('11','ImageResize');

INSERT INTO `modx_categories` VALUES ('12','registration');

INSERT INTO `modx_categories` VALUES ('13','Js');


# --------------------------------------------------------

#
# Table structure for table `modx_document_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_document_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_document_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_document_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_documentgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_documentgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_documentgroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_ep_choice`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_choice`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_choice` (
  `idChoice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPoll` int(10) unsigned NOT NULL,
  `Title` varchar(128) NOT NULL,
  `Sorting` tinyint(3) unsigned NOT NULL,
  `Votes` int(11) NOT NULL,
  PRIMARY KEY (`idChoice`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_choice`
#

INSERT INTO `modx_ep_choice` VALUES ('1','1','Лыжные гонки','1','755');

INSERT INTO `modx_ep_choice` VALUES ('2','1','Хоккей','2','10');

INSERT INTO `modx_ep_choice` VALUES ('3','1','Футбол','3','41');

INSERT INTO `modx_ep_choice` VALUES ('4','1','Теннис','4','5');

INSERT INTO `modx_ep_choice` VALUES ('5','1','Бег','5','29');

INSERT INTO `modx_ep_choice` VALUES ('6','1','Плавание','6','12');

INSERT INTO `modx_ep_choice` VALUES ('7','1','Баскетбол','7','15');

INSERT INTO `modx_ep_choice` VALUES ('8','1','Другой','8','34');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_language`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_language`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_language` (
  `idLang` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LangShort` char(3) NOT NULL,
  `LangName` varchar(256) NOT NULL,
  PRIMARY KEY (`idLang`),
  UNIQUE KEY `uniqLang` (`LangShort`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_language`
#

INSERT INTO `modx_ep_language` VALUES ('1','ru','ru');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_poll`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_poll`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_poll` (
  `idPoll` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(128) NOT NULL,
  `isActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idPoll`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_poll`
#

INSERT INTO `modx_ep_poll` VALUES ('1','Ваш любимый вид спорта:','1','2013-07-19 00:36:15','0000-00-00 00:00:00');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_translation`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_translation`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_translation` (
  `idPoll` int(10) unsigned NOT NULL,
  `idChoice` int(10) unsigned NOT NULL DEFAULT '0',
  `idLang` int(10) unsigned NOT NULL,
  `TextValue` varchar(2048) NOT NULL,
  PRIMARY KEY (`idChoice`,`idLang`,`idPoll`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_translation`
#

INSERT INTO `modx_ep_translation` VALUES ('1','0','1','Ваш любимый вид спорта:');

INSERT INTO `modx_ep_translation` VALUES ('1','1','1','Лыжные гонки');

INSERT INTO `modx_ep_translation` VALUES ('1','2','1','Хоккей');

INSERT INTO `modx_ep_translation` VALUES ('1','3','1','Футбол');

INSERT INTO `modx_ep_translation` VALUES ('1','4','1','Теннис');

INSERT INTO `modx_ep_translation` VALUES ('1','5','1','Бег');

INSERT INTO `modx_ep_translation` VALUES ('1','6','1','Плавание');

INSERT INTO `modx_ep_translation` VALUES ('1','7','1','Баскетбол');

INSERT INTO `modx_ep_translation` VALUES ('1','8','1','Другой');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_userip`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_userip`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_userip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPoll` int(10) unsigned NOT NULL,
  `ipAddress` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_userip`
#
#
# ВМ Database Dump
# MODX Version:1.2.1
# 
# Host: 
# Generation Time: 19-12-2019 10:29:28
# Server version: 5.6.41-84.1
# PHP Version: 5.6.40
# Database: `stroyzas_vesenni`
# Description: 
#

# --------------------------------------------------------

#
# Table structure for table `modx_active_user_locks`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_user_locks`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_user_locks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `elementType` int(1) NOT NULL DEFAULT '0',
  `elementId` int(10) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_element_id` (`elementType`,`elementId`,`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=265 DEFAULT CHARSET=latin1 COMMENT='Contains data about locked elements.';

#
# Dumping data for table `modx_active_user_locks`
#


# --------------------------------------------------------

#
# Table structure for table `modx_active_user_sessions`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_user_sessions`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_user_sessions` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data about valid user sessions.';

#
# Dumping data for table `modx_active_user_sessions`
#

INSERT INTO `modx_active_user_sessions` VALUES ('630930916477994b0922c1ef188b4a12','1','1576740568','185.7.87.12, 185.7.87.12');


# --------------------------------------------------------

#
# Table structure for table `modx_active_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_users` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `action` varchar(10) NOT NULL DEFAULT '',
  `id` int(10) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data about last user action.';

#
# Dumping data for table `modx_active_users`
#

INSERT INTO `modx_active_users` VALUES ('630930916477994b0922c1ef188b4a12','1','admin','1576740568','93',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=cp1251 COMMENT='Categories to be used snippets,tv,chunks, etc';

#
# Dumping data for table `modx_categories`
#

INSERT INTO `modx_categories` VALUES ('1','Demo Content');

INSERT INTO `modx_categories` VALUES ('2','Login');

INSERT INTO `modx_categories` VALUES ('3','Manager and Admin');

INSERT INTO `modx_categories` VALUES ('4','Search');

INSERT INTO `modx_categories` VALUES ('5','Navigation');

INSERT INTO `modx_categories` VALUES ('6','Content');

INSERT INTO `modx_categories` VALUES ('7','Forms');

INSERT INTO `modx_categories` VALUES ('8','Template');

INSERT INTO `modx_categories` VALUES ('9','Изменяемые данные');

INSERT INTO `modx_categories` VALUES ('10','System');

INSERT INTO `modx_categories` VALUES ('11','ImageResize');

INSERT INTO `modx_categories` VALUES ('12','registration');

INSERT INTO `modx_categories` VALUES ('13','Js');


# --------------------------------------------------------

#
# Table structure for table `modx_document_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_document_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_document_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_document_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_documentgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_documentgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_documentgroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_ep_choice`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_choice`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_choice` (
  `idChoice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPoll` int(10) unsigned NOT NULL,
  `Title` varchar(128) NOT NULL,
  `Sorting` tinyint(3) unsigned NOT NULL,
  `Votes` int(11) NOT NULL,
  PRIMARY KEY (`idChoice`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_choice`
#

INSERT INTO `modx_ep_choice` VALUES ('1','1','Лыжные гонки','1','755');

INSERT INTO `modx_ep_choice` VALUES ('2','1','Хоккей','2','10');

INSERT INTO `modx_ep_choice` VALUES ('3','1','Футбол','3','41');

INSERT INTO `modx_ep_choice` VALUES ('4','1','Теннис','4','5');

INSERT INTO `modx_ep_choice` VALUES ('5','1','Бег','5','29');

INSERT INTO `modx_ep_choice` VALUES ('6','1','Плавание','6','12');

INSERT INTO `modx_ep_choice` VALUES ('7','1','Баскетбол','7','15');

INSERT INTO `modx_ep_choice` VALUES ('8','1','Другой','8','34');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_language`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_language`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_language` (
  `idLang` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LangShort` char(3) NOT NULL,
  `LangName` varchar(256) NOT NULL,
  PRIMARY KEY (`idLang`),
  UNIQUE KEY `uniqLang` (`LangShort`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_language`
#

INSERT INTO `modx_ep_language` VALUES ('1','ru','ru');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_poll`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_poll`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_poll` (
  `idPoll` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(128) NOT NULL,
  `isActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idPoll`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_poll`
#

INSERT INTO `modx_ep_poll` VALUES ('1','Ваш любимый вид спорта:','1','2013-07-19 00:36:15','0000-00-00 00:00:00');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_translation`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_translation`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_translation` (
  `idPoll` int(10) unsigned NOT NULL,
  `idChoice` int(10) unsigned NOT NULL DEFAULT '0',
  `idLang` int(10) unsigned NOT NULL,
  `TextValue` varchar(2048) NOT NULL,
  PRIMARY KEY (`idChoice`,`idLang`,`idPoll`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_translation`
#

INSERT INTO `modx_ep_translation` VALUES ('1','0','1','Ваш любимый вид спорта:');

INSERT INTO `modx_ep_translation` VALUES ('1','1','1','Лыжные гонки');

INSERT INTO `modx_ep_translation` VALUES ('1','2','1','Хоккей');

INSERT INTO `modx_ep_translation` VALUES ('1','3','1','Футбол');

INSERT INTO `modx_ep_translation` VALUES ('1','4','1','Теннис');

INSERT INTO `modx_ep_translation` VALUES ('1','5','1','Бег');

INSERT INTO `modx_ep_translation` VALUES ('1','6','1','Плавание');

INSERT INTO `modx_ep_translation` VALUES ('1','7','1','Баскетбол');

INSERT INTO `modx_ep_translation` VALUES ('1','8','1','Другой');


# --------------------------------------------------------

#
# Table structure for table `modx_ep_userip`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_ep_userip`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_ep_userip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPoll` int(10) unsigned NOT NULL,
  `ipAddress` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_ep_userip`
#
