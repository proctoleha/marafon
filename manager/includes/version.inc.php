<?php
$modx_version      = '1.4.10'; // Current version number
$modx_release_date = 'Nov 05, 2019'; // Date of release
$modx_branch       = 'Evolution'; // Codebase name
$modx_full_appname = "{$modx_branch} {$modx_version} ({$modx_release_date})";
