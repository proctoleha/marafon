<?php

require_once 'Model.php';
require_once 'Create_url.php';
require_once 'Config.php';

if (!isset($_SESSION['edit']) || !$_SESSION['edit']) {
    $url = Create_url::base_url() . 'login.php';
    header("Location: $url");
    exit();
}

if (!$_GET && !$_POST) {
    $res = start_page();
} else {
    $t = null;
    if ($_GET) {
        $t = $_GET['get_data'];
    } else {
        if ($_POST) {
            $t = $_POST['get_data'];
        }
    }
    if ($t) {
        $res = $t();
    } else {
        exit('Error! No data from working!');
    }
}

function menu()
{
    $model = new Model();
    return $model->menu_model();
}

function del()
{
    $model = new Model();
    $model->del_model($_POST);
}

function edit_data()
{
    $model = new Model();
    foreach ($_POST as $k => $v) {
        $arr[$k] = htmlspecialchars(trim($v));
    }
    $result = $model->edit_data_model($arr);
    if ($result['error']) {
        echo json_encode($result);
    } else {
        echo json_encode(['success' => true]);
    }
}

function delete_num()
{
    $id = $_POST['id'];
    $model = new Model();
    $model->deleteItem('mar_numbers', $id);
}

function set_number()
{
    $min = (int)$_POST['min_number'];
    $max = (int)$_POST['max_number'];
    $id = (int)$_POST['num_id'];
    if ($min <= 0 || $max <= 0) {
        echo json_encode(['error' => 'Неверное минимальное, или максимальное значение!']);
        return;
    }
    if ($max < $min) {
        echo json_encode(['error' => 'Максимальное значение не может быть меньше минимального!']);
        return;
    }
    $year_id = (int)$_POST['year_id'];
    $group_id = (int)$_POST['group_id'];
    $model = new Model();
    $check = $model->checkNumber($min, $max, $year_id, $id);
    if ($check['error']) {
        echo json_encode(['error' => $check['error']]);
        return;
    }
    $model->setNumber($min, $max, $year_id, $group_id, $id);
    echo json_encode(['success' => true]);
}

function get_group($data = array())
{
    $model = new Model();

    $cat = !$data ? $_GET['cat'] : $data['cat'];
    $year = !$data ? $_GET['year'] : $data['year'];
    $id = !$data ? $_GET['id'] : $data['id'];

    if ((int)$year) {
        if ($cat == 'child' || $cat == 'adults') {
            $arr = $model->get_global_group($cat, $year, $id);
            $groups = [];
            if ($arr) {
                foreach ($arr as $item) {
                    $groups[$item['num_g']][] = $item;
                }
            }
            if (!$data) {
                $edit_form = view_include('form_edit_data_view', [
                    'action' => Create_url::base_url('edit_data')
                ]);
                $title = $model->getTitle($id);
                return view_include('get_group_view', compact('edit_form', 'arr', 'cat', 'year', 'groups', 'title'));
            }
            return $arr;
        }
    }
    return false;
}

function start_page()
{
    $model = new Model();

    $date = $model->get_current_year(true);

    $year = $date['year'];
    $year_id = $date['id'];
    $date_stop = $date['date_stop'];
    $title = $date['title'];

    $all_groups = $model->get_all_groups_model();

    $numbers = $model->getNumbers($year_id);

    $groups_alias = [
        'child' => 'Детский марафон',
        'adults' => 'Взрослый марафон',
        'men' => 'Мужчины',
        'woomen' => 'Женщины',
        'boys' => 'Юноши',
        'girls' => 'Девушки'
    ];

    return view_include('start_page_view',
        compact('year', 'date_stop', 'all_groups', 'groups_alias', 'year_id', 'numbers', 'title'));
}

function set_year()
{
    $model = new Model();
    $year = (int)$_POST['year'];
    $is_new = !empty($_POST['is_new']);
    $is_copy_num = !empty($_POST['is_copy_num']);
    $title = $_POST['title'];
    if ($year >= date('Y', time())) {
        $arr_date = explode('.', $_POST['date_stop']);

        if (count($arr_date) == 2) {
            if ((int)$arr_date[0] < 32 && (int)$arr_date[0] > 0 && (int)$arr_date[1] < 13 &&
                (int)$arr_date[1] > 0) {
                $date_stop = strtotime("$year-$arr_date[1]-$arr_date[0]");
            }
        }
        $model->set_year_model($year, $date_stop, $is_new, $title, $is_copy_num);
    }
    Create_url::redirect();
}

function set_length()
{
    $model = new Model();
    $length = (int)$_POST['length'];
    $id = (int)$_POST['id'];
    if ($length) {
        $model->set_length_model($id, $length);
    }
    echo json_encode(['success' => true]);
}

function create_excel_file()
{
    $data = get_group($_POST);
    require_once './excel/PHPExcel.php';
    $phpexcel = new PHPExcel;
    $num_group = $_POST['num'];
    $file_name = $_POST['cat'] . '_' . $num_group . '_' . $_POST['year'] . '.xlsx';
    $file_name = '../assets/files/' . $file_name;
    if (is_file($file_name)) {
        unlink($file_name);
    }
    $page = $phpexcel->setActiveSheetIndex(0);
    $page->setCellValue("A1", "№ гр.");
    $page->setCellValue("B1", "ФИО");
    $page->setCellValue("C1", "Д/р, возраст");
    $page->setCellValue("D1", "Км.");
    $page->setCellValue("E1", "Разряд");
    $page->setCellValue("F1", "Регион");
    $page->setCellValue("G1", "Телефон");
    $page->setCellValue("H1", "Email");
    $page->setCellValue("I1", "Пол");
    $page->setCellValue("J1", "Номер участника");
    $page->setCellValue("K1", "Район");
    $page->setCellValue("L1", "Организация");

    foreach ($data as $k => $v) {
        if ($v['num_g'] == $num_group) {
            $page->setCellValueExplicit("A" . ($k + 2), $v["num_g"] . 'гр.', PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("B" . ($k + 2), $v["name"], PHPExcel_Cell_DataType::TYPE_STRING);
            $age = ($_POST['year'] - 1) - substr($v["date_birth"], -4, 4);
            $page->setCellValueExplicit("C" . ($k + 2), $v["date_birth"] . " ($age)", PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("D" . ($k + 2), $v["length"], PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("E" . ($k + 2), $v["sports_category"], PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("F" . ($k + 2), $v["region"], PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("G" . ($k + 2), $v["phone"], PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("H" . ($k + 2), $v["email"], PHPExcel_Cell_DataType::TYPE_STRING);
            $sex = $v['sex'] == 'm' ? 'М' : 'Ж';
            $page->setCellValueExplicit("I" . ($k + 2), $sex, PHPExcel_Cell_DataType::TYPE_STRING);
            if (!$v['num']) {
                $num = 'Не назначено';
            } else {
                $num = $v['num'];
            }
            $page->setCellValueExplicit("J" . ($k + 2), $num, PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("K" . ($k + 2), $v["district"], PHPExcel_Cell_DataType::TYPE_STRING);
            $page->setCellValueExplicit("L" . ($k + 2), $v["org"], PHPExcel_Cell_DataType::TYPE_STRING);
        }

    }

    $page->getColumnDimension('A')->setWidth(5);
    $page->getColumnDimension('B')->setWidth(40);
    $page->getColumnDimension('C')->setWidth(20);
    $page->getColumnDimension('D')->setWidth(5);
    $page->getColumnDimension('E')->setWidth(20);
    $page->getColumnDimension('F')->setWidth(20);
    $page->getColumnDimension('G')->setWidth(15);
    $page->getColumnDimension('I')->setWidth(5);
    $page->getColumnDimension('J')->setWidth(15);

    $g = $_POST['cat'] == 'child' ? 'Дети' : 'Взрослые';

    $page->setTitle($_POST['year'] . " " . $g);

    $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');

    $objWriter->save($file_name);

    if (is_file($file_name)) {
        echo 1;
    } else {
        echo 0;
    }

}

function view_include($file_name, $vars = array())
{
    $file_name = 'view/' . $file_name . '.php';
    foreach ($vars as $k => $v) {
        $$k = $v;
    }
    ob_start();
    include $file_name;
    return ob_get_clean();
}
