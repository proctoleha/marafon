<?

class Mail
{
    private $from = '';
    private $from_name = "";
    private $type = "text/html";
    private $encoding = "utf-8";

    public function __construct()
    {
        $this->from = Config::$admin_email;
    }

    /* Изменение обратного e-mail адреса */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /* Изменение имени в обратном адресе */
    public function setFromName($from_name)
    {
        $this->from_name = $from_name;
    }

    /* Изменение типа содержимого письма */
    public function setType($type)
    {
        $this->type = $type;
    }

    /* Изменение кодировки письма */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /* Метод отправки письма */
    public function send($to, $subject, $message)
    {
        $from = "=?utf-8?B?" . base64_encode($this->from_name) . "?=" . " <" . $this->
            from . ">"; // Кодируем обратный адрес (во избежание проблем с кодировкой)
        $headers = "From: " . $from . "\r\nReply-To: " . $from . "\r\nContent-type: " .
            $this->type . "; charset=" . $this->encoding . "\r\n"; // Устанавливаем необходимые заголовки письма

        $subject = "=?utf-8?B?" . base64_encode($subject) . "?="; // Кодируем тему (во избежание проблем с кодировкой)

        return mail($to, $subject, $message, $headers); // Отправляем письмо и возвращаем результат
    }

}
