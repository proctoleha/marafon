<?php

require_once 'Model.php';
require_once 'Mail.php';
require_once 'Config.php';


if (!empty($_POST['name_f'])) {
	$f = $_POST['name_f'];
	if (function_exists($f)) $f();
}


 function get_data_length_group()
{
    $model = new Model();
    $sex = $_POST['sex'];
    $age = $model->get_year(false, $_POST['date_birth']);
    if ($age >= 6 && $age < 18) {
        echo json_encode($model->get_data_length_group_model($age, $sex));
    } else {
        $result = [];
        $result['year'] = $model->get_current_year();
        echo json_encode($result);
    }
}

function reg()
{
    $model = new Model();

    foreach ($_POST as $k => $v)
    {
        if ($k != 'name_f' && $k != 'sex') // $name $phone $email $date_birth $sports_category $id_g

            $arr[$k] = trim(htmlspecialchars($v));
    }

    $arr_date = explode('.', $_POST['date_birth']);

    if (count($arr_date) == 3)
    {
        $full_year = $model->get_year(false, $_POST['date_birth']);

        $num = $model->max_num_model($full_year);

        $data = $model->set_reg_model($arr, ++$num);

        if ($data['res'])
        {
            $year = $model->get_current_year();

            $mail = new Mail;

            $sub = "Регистрация для участия в Весеннем марафоне $year";

            $name_from = "Весенний марафон";

            $mail->setFromName($name_from);

            $msg = view_include('email_view', array(
                'name' => $arr['name'],
                'year' => $year,
                'num' => $data['num']));

            $mail->send($arr['email'], $sub, $msg);
            $mail->send(Config::$admin_email, $sub, $msg);

            echo view_include('email_view', array(
                'msg_site' => '',
                'year' => $year,
                'num' => $data['num']));
        }
        else
            echo $data['res'] == 0 ? 0 : -1;
    }
}

function get_group()
{
    $model = new Model();

    $cat = $_POST['cat'];
    $year = $_POST['year'];

    if ((int)$year)
    {
        if ($cat == 'child' || $cat == 'adults')
        {
            $arr = $model->get_global_group($cat, $year);
            echo view_include('get_group_view_frontend', array(
                'arr' => $arr,
                'cat' => $cat,
                'year' => $year));
        }
    }
}

function check_dates()
{
    $model = new Model;

    if (!$model->check_dates_model())
        echo view_include('end_reg_view');
    else
        echo 1;
}

function count_reg()
{
    $model = new Model;
    $res = $model->count_reg_model();
    $res['year'] = get_cur_year();
    echo json_encode($res);
}

function get_cur_year()
{
    $model = new Model;
    $y = $model->get_current_year();
    return $y;
}

function echo_get_cur_year()
{
    echo get_cur_year();
}

function view_include($file_name, $vars = array())
{
    $file_name = 'view/' . $file_name . '.php';
    foreach ($vars as $k => $v)
        $$k = $v;

    ob_start();
    include $file_name;
    return ob_get_clean();
}
