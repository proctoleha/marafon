<?php

class Create_url
{
    public static function base_url($controller = '')
    {
        $path = 'http://' . $_SERVER["SERVER_NAME"] . '/reg/';
        if ($controller)
            $path .= 'controller.php';
        return $path;
    }
    
    public static function redirect ()
    {
        header("Location: $_SERVER[HTTP_REFERER]");
    }  
    
}
