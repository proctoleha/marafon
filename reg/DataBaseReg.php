<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';

class DataBaseReg
{
    private static $db = null; // Единственный экземпляр класса, чтобы не создавать множество подключений
    private $mysqli; // Идентификатор соединения
    private $sym_query = "?"; // "Символ значения в запросе"

    /* Получение экземпляра класса. Если он уже существует, то возвращается, если его не было, то создаётся и возвращается (паттерн Singleton) */
    public static function getDB()
    {
        if (self::$db == null) {
            self::$db = new DataBaseReg();
        }
        return self::$db;
    }

    /* private-конструктор, подключающийся к базе данных, устанавливающий локаль и кодировку соединения */
    private function __construct()
    {
        $this->mysqli = new mysqli($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASSWD'],
            $_ENV['DB_NAME']);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /* Вспомогательный метод, который заменяет "символ значения в запросе" на конкретное значение, которое проходит через "функции безопасности" */
    private function get_query($query, $params)
    {
        if ($params) {
            $count = count($params);
            for ($i = 0; $i < $count; $i++) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'" . $this->mysqli->real_escape_string($params[$i]) . "'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    /* SELECT-метод, возвращающий таблицу результатов */
    public function select($query, $params = false)
    {
        $result_set = $this->mysqli->query($this->get_query($query, $params));
        if (!$result_set) {
            return false;
        }
        return $this->result_set_to_array($result_set);
    }

    /* SELECT-метод, возвращающий одну строку с результатом */
    public function select_row($query, $params = false)
    {
        $result_set = $this->mysqli->query($this->get_query($query, $params));
        if ($result_set->num_rows != 1) {
            return false;
        } else {
            return $result_set->fetch_assoc();
        }
    }

    /* SELECT-метод, возвращающий значение из конкретной ячейки */
    public function select_cell($query, $params = false)
    {
        $result_set = $this->mysqli->query($this->get_query($query, $params));
        if ((!$result_set) || ($result_set->num_rows != 1)) {
            return false;
        } else {
            $arr = array_values($result_set->fetch_assoc());
            return $arr[0];
        }
    }

    /* НЕ-SELECT методы (INSERT, UPDATE, DELETE). Если запрос INSERT, то возвращается id последней вставленной записи */
    public function query($query, $params = false)
    {
        $success = $this->mysqli->query($this->get_query($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) {
                return true;
            } else {
                return $this->mysqli->insert_id;
            }
        } else {
            return false;
        }
    }

    /* Преобразование result_set в двумерный массив */
    private function result_set_to_array($result_set)
    {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }
}
