<?php
if ($_ENV['DEV']) {
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}

require_once 'controller.php';
$base_url = Create_url::base_url();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8" />
	<title></title>
	<link href="<?= $base_url ?>style.css" rel="stylesheet" />
    <script src="<?= $base_url ?>jquery.js"></script>
    <script src="<?= $base_url ?>js.js"></script>
</head>

<body>

<div class="wrapper">

	<header class="header">
		<h2>Весенний марафон: админка для регистрации</h2>
	</header><!-- .header-->

	<div class="middle">

		<div class="container">
			<main class="content">
				<strong><?= $res ?></strong>			
            </main><!-- .content -->
		</div><!-- .container-->

		<aside class="left-sidebar">
			
            <ul id="menu">
            
            <?php $menu = menu() ?>
            
                <li><a href="<?= $base_url ?>">Год проведения марафона, группы</a></li>
                
                
                <?php foreach ($menu as $v) : ?>
                
                <li>
                    <a href="?get_data=get_group&cat=adults&year=<?= $v['year'] ?>&id=<?= $v['id'] ?>">Взрослые, <?= $v['title'] ?>, <?= $v['year'] ?></a>
                </li>

                <li>
                    <a href="?get_data=get_group&cat=child&year=<?= $v['year'] ?>&id=<?= $v['id'] ?>">Дети, <?= $v['title'] ?>, <?= $v['year'] ?></a>
                </li>

                <?php endforeach ?>
                
            </ul>
            
        </aside><!-- .left-sidebar -->

	</div><!-- .middle-->

</div><!-- .wrapper -->

</body>
</html>