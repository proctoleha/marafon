$(function () {
    $('.next_show').click(function () {
        $(this).next().toggle(300);
    });

    $('.null-number').click(function () {
        let url = $(this).data('url');
        let action = $(this).data('action');
        let id = $(this).data('id');
        $.post(url, {get_data: action, id: id}, function () {
            window.location.reload();
        })
    });

    $('.edit_data, .edit_number').submit(function () {
        $.post(this.action, $(this).serialize(), function (data) {
            if (data.error !== undefined) {
                alert(data.error);
                return false;
            }
            location.reload();
        }, 'json');
        return false;
    });

    $('.del').click(function () {
        if (confirm('Удалить участника? Вы уверены?')) {
            var form = $(this).prev('form');
            $.post(form.attr('action'), form.serialize(), function () {
                location.reload();
            });
        }
    });

    $('.gen_excel').click(function (e) {
        e.preventDefault();
        var a = $(this);
        var obj = {
            year: a.attr('data-year'),
            cat: a.attr('data-cat'),
            get_data: 'create_excel_file',
            num: a.attr('data-num')
        };
        $.post(this.href, obj, function (data) {
            if (data === '1')
                window.location = a.attr('data-d');
            else
                alert('Произошла неизвестная ошибка. Сообщите админу.');
        });
    });

});