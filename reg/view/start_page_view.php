<?php
/**
 * @var array $all_groups
 * @var array $groups_alias
 * @var int $year_id
 * @var int $year
 * @var array $numbers
 * @var string $date_stop
 * @var string $title
 */
require_once dirname(__DIR__) . '/Create_url.php'
?>

<div class="article">
    <h1>Год проведения марафона: <?= $year ?></h1>

    <form method="post" action="<?= Create_url::base_url('c') ?>">
        <label>В поле Год строго 4 цифры</label><br/>
        <input required="" name="year" value="<?= $year ?>" class="txt" placeholder="Year"/><br/>
        <label>Дата окончания рег-ции ОБЯЗАТЕЛЬНО в формате 12.03 (4 цифры с точкой)</label><br/>
        <input style="width: 80px;" name="date_stop" value="<?= date('d.m', $date_stop) ?>" class="txt" required=""
               placeholder="Date stop"/>
        <p>
        <label for="title-event">Название события (например Марафон, Контрольная тренировка)</label><br>
        <input value="<?= $title ?>" id="title-event" name="title" required></p>
        <p style="margin-top: 5px;">
            <label for="checkbox-new">
                <input id="checkbox-new" name="is_new" type="checkbox">
                Новое событие, или нет (если чекбокс не отмечен, то будет изменена только дата окончания регистрации)
            </label>
        </p>
        <p style="margin-top: 5px;">
            <label for="checkbox-copy">
                <input id="checkbox-copy" name="is_copy_num" type="checkbox">
                Скопировать номера участников с прошлого события?
            </label>
        </p>
        <input type="hidden" name="get_data" value="set_year"/>
        <p style="margin-top: 5px;">
            <input type="submit" value="Изменить данные" class="btn"/>
        </p>


    </form>

</div>

<div class="article">
    <h1>Группы, дистанции</h1>
    <?php foreach ($all_groups as $group_global => $data) : ?>
        <h2><?= $groups_alias[$group_global] ?></h2>
        <table>
            <tr>
                <th>№ гр.</th>
                <th>Возраст<br/>на 31.12.<?= substr($year, -2, 2) - 1 ?></th>
                <th>Года</th>
                <th>Дистанция</th>
                <th>Номера участников</th>
            </tr>
            <?php foreach ($data as $v) : ?>
                <tr>
                    <td><?= $v['num'] ?></td>

                    <td><?= $v['start_age'] . ' - ' . $v['end_age'] ?></td>

                    <td><?= $v['start_year'] . ' / ' . $v['end_year'] ?></td>

                    <td>
                        <?php $sex = array_keys($v['distanse']) ?>
                        <span class="next_show"><?= $groups_alias[$sex[0]] . ': ' . $v['distanse'][$sex[0]]['length'] ?>км.</span>

                        <form class="edit_data" style="margin-bottom: 3px;" method="post"
                              action="<?= Create_url::base_url('c') ?>">
                            <input style="width: 20px;" name="length" value="" class="txt"/>
                            <input type="hidden" name="get_data" value="set_length"/>
                            <input type="hidden" name="id" value="<?= $v['distanse'][$sex[0]]['id'] ?>"/>
                            <input type="submit" value="Изменить" class="btn"/>
                        </form>

                        <?php if ($v['distanse'][$sex[1]]) : ?>
                            <span class="next_show"><?= $groups_alias[$sex[1]] . ': ' . $v['distanse'][$sex[1]]['length'] ?>км.</span>
                            <form class="edit_data" method="post" action="<?= Create_url::base_url('c') ?>">
                                <input style="width: 20px;" name="length" value="" class="txt"/>
                                <input type="hidden" name="get_data" value="set_length"/>
                                <input type="hidden" name="id" value="<?= $v['distanse'][$sex[1]]['id'] ?>"/>
                                <input type="submit" value="Изменить" class="btn">
                            </form>
                        <?php endif ?>

                    </td>

                    <td>
                        <?php
                        $group_id = $v['distanse'][$sex[0]]['id'];
                        if (!empty($numbers[$year_id][$group_id]['min'])) {
                            $min = $numbers[$year_id][$group_id]['min'];
                        } else {
                            $min = 0;
                        }
                        if (!empty($numbers[$year_id][$group_id]['max'])) {
                            $max = $numbers[$year_id][$group_id]['max'];
                        } else {
                            $max = 0;
                        }
                        if (!empty($numbers[$year_id][$group_id]['id'])) {
                            $num_id = $numbers[$year_id][$group_id]['id'];
                        } else {
                            $num_id = 0;
                        }
                        ?>
                        <p>Начало диапазона (юноши): <?= $min ?></p>
                        <p>Конец диапазона (юноши): <?= $max ?></p>
                        <span class="next_show">Изменить</span>
                        <form style="display: none" class="edit_number" method="post"
                              action="<?= Create_url::base_url('c') ?>">
                            <label><strong>Начало</strong></label>
                            <p style="margin-bottom: 10px;">
                                <input name="min_number" value=<?= $min ?>>
                            </p>
                            <label><strong>Конец</strong></label>
                            <p style="margin-bottom: 10px;">
                                <input name="max_number" value=<?= $max ?>>
                            </p>
                            <input type="hidden" name="get_data" value="set_number">
                            <input type="hidden" name="year_id" value="<?= $year_id ?>">
                            <input type="hidden" name="group_id" value="<?= $group_id ?>">
                            <input type="hidden" name="num_id" value="<?= $num_id ?>">
                            <input type="submit" value="Ok" class="btn">
                            <?php if ($num_id) : ?>
                                <input type="button" value="Обнулить" data-url="<?= Create_url::base_url('c') ?>"
                                       data-id="<?= $num_id ?>" data-action="delete_num" class="btn null-number">
                            <?php endif ?>
                        </form>
                        <?php if(!empty($v['distanse'][$sex[1]]['id'])) : ?>
                            <?php
                            $group_id = $v['distanse'][$sex[1]]['id'];
                            if (!empty($numbers[$year_id][$group_id]['min'])) {
                                $min = $numbers[$year_id][$group_id]['min'];
                            } else {
                                $min = 0;
                            }
                            if (!empty($numbers[$year_id][$group_id]['max'])) {
                                $max = $numbers[$year_id][$group_id]['max'];
                            } else {
                                $max = 0;
                            }
                            if (!empty($numbers[$year_id][$group_id]['id'])) {
                                $num_id = $numbers[$year_id][$group_id]['id'];
                            } else {
                                $num_id = 0;
                            }
                            ?>
                            <p>Начало диапазона (девушки): <?= $min ?></p>
                            <p>Конец диапазона (девушки): <?= $max ?></p>
                            <span class="next_show">Изменить</span>
                            <form style="display: none" class="edit_number" method="post"
                                  action="<?= Create_url::base_url('c') ?>">
                                <label><strong>Начало</strong></label>
                                <p style="margin-bottom: 10px;">
                                    <input name="min_number" value=<?= $min ?>>
                                </p>
                                <label><strong>Конец</strong></label>
                                <p style="margin-bottom: 10px;">
                                    <input name="max_number" value=<?= $max ?>>
                                </p>
                                <input type="hidden" name="get_data" value="set_number">
                                <input type="hidden" name="year_id" value="<?= $year_id ?>">
                                <input type="hidden" name="group_id" value="<?= $group_id ?>">
                                <input type="hidden" name="num_id" value="<?= $num_id ?>">
                                <input type="submit" value="Ok" class="btn">
                                <?php if ($num_id) : ?>
                                    <input type="button" value="Обнулить" data-url="<?= Create_url::base_url('c') ?>"
                                           data-id="<?= $num_id ?>" data-action="delete_num" class="btn null-number">
                                <?php endif ?>
                            </form>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach ?>

        </table>


    <?php endforeach ?>


</div>