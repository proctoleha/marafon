<?php
require_once dirname(dirname(dirname(__FILE__))) . '/reg/Csrf.php';
$csrf = new Csrf();
$token_id = $csrf->getTokenId();
$token_value = $csrf->getToken();
?>
<div id="reg_form">
    <h2>Форма регистрации участников Весеннего марафона 2021</h2>
    <p>
        Уважаемые участники! При регистрации для участия в Весеннем марафоне 2021 вы подтверждаете своё участие в соревнованиях, за вами закрепляется стартовый номер! Огромная просьба в случае если вы зарегистрировались, но по каким-то причинам не сможете принять участие в соревнованиях, просим сообщить в электронном письме на почту info@vesenniymarafon.ru, либо позвонить по телефону +7 915-167-34-82 не позднее 10-00 05.02.2021.
    </p>
    <hr/>
    <hr/>
    <form class="form" id="reg_data" action="/reg/" method="post">
        <p>
            <input name="name" required="" placeholder="ФИО полностью" type="text"/>
        </p>
        <p style="padding-right: 330px;">
            <input class="radio" type="radio" name="sex" value="m"/>
            <span style="left: 94px; top: 15px; display: block; position: absolute;">Мужской пол</span>
        </p>
        <p style="padding-right: 330px;">
            <input class="radio" type="radio" name="sex" value="w"/>
            <span style="left: 94px; top: 15px; display: block; position: absolute;">Женский пол</span>
        </p>
        <p>
            <input name="phone" required="" placeholder="Телефон" type="text"/>
        </p>
        <p>
            <input name="email" required="" placeholder="E-mail" type="text"/>
        </p>
        <p>
            <input name="date_birth" required="required" placeholder="Д/p дд.мм.гггг" type="text"/>
        </p>
        <p>
            <select name="region">
                <option value="">Выберите регион</option>
                <option>Владимирская обл.</option>
                <option>Вологодская обл.</option>
                <option>Ивановская обл.</option>
                <option>Карелия респ.</option>
                <option>Кировская обл.</option>
                <option>Костромская обл.</option>
                <option>Ленинградская обл.</option>
                <option>г.Москва</option>
                <option>Московская обл.</option>
                <option>Мурманская обл.</option>
                <option>Нижегородская обл.</option>
                <option>Новгородская обл.</option>
                <option>Псковская обл.</option>
                <option>г.Санкт-Петербург</option>
                <option>Ярославская обл.</option>
                <option>Моего региона нет в списке</option>
            </select>
        </p>
        <p>
            <input type="text" name="district" placeholder="Район">
        </p>
        <p>
            <input type="text" name="org" placeholder="Организация">
        </p>
        <p>
            <select name="sports_category">
                <option value="0">Спортивный разряд</option>
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>КМС</option>
                <option>МС</option>
                <option>МСМК</option>
                <option>ЗМС</option>
            </select>
        </p>

        <p style="margin-right: 75px;">Дистанция (сначала выберите пол и д/р)</p>

        <p style="padding-right: 330px;">
            <input disabled="disabled" class="radio dist0" type="radio" name="id_g" value=""/>
            <span style="left: 94px; top: 15px; display: block; position: absolute;"></span>
        </p>
        <p style="padding-right: 330px;">
            <input disabled="disabled" class="radio dist1" type="radio" name="id_g" value=""/>
            <span style="left: 94px; top: 15px; display: block; position: absolute;"></span>
        </p>
        <input type="hidden" name="name_f" value="reg"/>
        <p style="padding-left: 150px;">
            <input class="sub" type="submit" value="Зарегистрироваться"/>
        </p>
    </form>
    <p></p>
    <p>
        <img src="assets/images/VM_Images/Document.png" style="vertical-align: middle;" height="36" width="36"/>&nbsp;&nbsp;
        <a class="get_g" data-y="" data-age="child" href="#">Участники марафона (дети)</a>
    </p>
</div>
<script src="tmp/js/jquery.inputmask.js" type="text/javascript"></script>
<script src="tmp/js/reg.js" type="text/javascript"></script>
