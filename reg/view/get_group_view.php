<?php
/**
 * @var string $cat
 * @var string $edit_form
 * @var string $title
 * @var string $year
 * @var array $arr
 * @var array $groups
 */
?>

<h1><?= $cat == 'child' ? 'Дети' : 'Взрослые' ?>, <?= $title ?>, <?= $year ?></h1>

<?php if($groups) : ?>
    <?php foreach($groups as $key => $group) : ?>
        <h3>Группа №<?= $key ?></h3>

        <table style="width: inherit;">
            <tr>
                <th style="width: 10px;">№ п/п</th>
                <th style="width: 30px;">№ гр.</th>

                <th style="width: 100px;">ФИО</th>
                <th style="width: 100px;">Д/р и полных лет на 31.12.<?= --$year ?></th>
                <th style="width: 20px;">Км.</th>
                <th style="width: 30px;">Разр.</th>
                <th style="width: 100px;">Регион</th>
                <th style="width: 80px;">Телефон</th>
                <th style="width: 100px;">Емейл</th>
                <th style="width: 20px;">Пол</th>
                <th>Номер участника</th>
                <th>Удалить участника</th>
            </tr>
            <?php $i = 1 ?>
            <?php foreach ($group as $k => $v) : ?>
                <tr>
                    <td><?= $i ?>)</td>
                    <td><?= $v['num_g'] ?>-я</td>
                    <td>
                        <span class="next_show"><?= $v['name'] ?></span>
                        <?= sprintf($edit_form, '', 'name', $v['name'], $v['id_p'], 'name') ?>
                    </td>
                    <td>
                <span class="next_show"><?= $v['date_birth'] ?> (<?= $full_year = $year - substr($v['date_birth'], -4,
                            4) ?>)</span>
                        <?= sprintf($edit_form, '', 'date_birth', $v['date_birth'], $v['id_p'], 'date_birth') ?>
                    </td>
                    <td><?= $v['length'] ?></td>
                    <td>
                        <span class="next_show"><?= $v['sports_category'] ?></span>
                        <?= sprintf($edit_form, '', 'sports_category', $v['sports_category'], $v['id_p'], 'sports_category') ?>
                    </td>
                    <td>
                        <span class="next_show"><?= $v['region'] ?></span>
                        <?= sprintf($edit_form, '', 'region', $v['region'], $v['id_p'], 'region') ?>
                    </td>
                    <td>
                        <span class="next_show"><?= $v['phone'] ?></span>
                        <?= sprintf($edit_form, '', 'phone', $v['phone'], $v['id_p'], 'phone') ?>
                    </td>
                    <td>
                        <span class="next_show"><?= $v['email'] ?></span>
                        <?= sprintf($edit_form, '', 'email', $v['email'], $v['id_p'], 'email') ?>
                    </td>
                    <td>
                        <?php $sex = $v['sex'] == 'm' ? 'М' : 'Ж' ?>
                        <span class="next_show"><?= $sex ?></span>
                        <?= sprintf($edit_form, 'class="short"', 'sex', $sex, $v['id_p'], 'sex') ?>
                    </td>
                    <td>
                        <?php
                        if (!$v['num']) {
                            $num = 'Не назначен';
                        } else {
                            $num = $v['num'];
                        }
                        ?>
                        <?= $num ?>
                        <span class="next_show">Изменить номер</span>
                        <?= sprintf($edit_form, 'class="num"', 'num', $v['num'], $v['id_p'], 'num') ?>
                    </td>
                    <td>
                        <form method="post" action="<?= Create_url::base_url('c') ?>">
                            <input type="hidden" name="num" value="<?= $v['num_p'] ?>"/>
                            <input type="hidden" name="full_year" value="<?= $full_year ?>"/>
                            <input type="hidden" name="id" value="<?= $v['id_p'] ?>"/>
                            <input type="hidden" name="get_data" value="del"/>
                        </form>
                        <img title="Удалить участника" alt="delete" class="del"
                             src="<?= Create_url::base_url() . 'del.png' ?>"/>
                    </td>
                </tr>
                <?php $i++ ?>
            <?php endforeach ?>
        </table>
        <p style="margin: 20px 0;">
            <?php $file_name = 'http://' . $_SERVER["SERVER_NAME"] . '/assets/files/' . $cat . '_' . $key. '_' . ++$year . '.xlsx' ?>
            <a class="gen_excel" data-num="<?= $key ?>" data-d="<?= $file_name ?>" data-year="<?= $year ?>" data-cat="<?= $cat ?>"
               href="<?= Create_url::base_url('c') ?>">Сгенерировать файл excel для группы № <?= $key ?></a>
        </p>
    <?php endforeach ?>
<?php endif ?>





