<h2>Участники <?= $cat == 'child' ? 'детского' : 'взрослого' ?>   марафона <?= $year ?></h2>
<table style="width: inherit;">
    <tr>
        <th style="width: 10px;">№ п/п</th>
        <th style="width: 30px;">№ гр.</th>
        
        <th style="width: 100px;">ФИО</th>
        <th style="width: 30px;">Км.</th>
        <th style="width: 50px;">Разряд</th>
        <th style="width: 50px;">Регион</th>
    </tr>
<? $i=1 ?>
<? foreach ($arr as $k => $v) : ?>
    <tr>
        <td><?= $i ?>)</td>
        <td><?= $v['num_g'] ?>-я</td>
        
        <td>
            <span><?= $v['name'] ?></span>
        </td>
        <td><?= $v['length'] ?> км.</td>
        
        <td>
            <span><?= (int)$v['sports_category'] ? $v['sports_category'] . ' разряд' : $v['sports_category'] ?> </span>
        </td>
        <td>
            <span><?= $v['region'] ?></span>  
        </td>
        <? $i++ ?>
    </tr>
<? endforeach ?>

</table>