<?

require_once 'DataBaseReg.php';

class Model
{
    private $db;

    public function __construct()
    {
        $this->db = DataBaseReg::getDB();
    }

    public function getNumber($year_id, $group_id, $participant_id)
    {
        $num = null;
        $sql = "SELECT min_num, max_num FROM mar_numbers WHERE mar_year_table_id = $year_id AND mar_groups_id = $group_id";
        if ($data = $this->db->select_row($sql)) {
            $sql = $this->numSql($group_id, $year_id);
            if (!$numbers_pre = $this->db->select($sql)) {
                $num = $data['min_num'];
            } else {
                foreach ($numbers_pre as $number) {
                    $numbers[] = $number['num'];
                }
                for ($i = $data['min_num']; $i <= $data['max_num']; $i++) {
                    if (!in_array($i, $numbers)) {
                        $num = $i;
                        break;
                    }
                }
            }
            if ($num) {
                $sql = "UPDATE mar_participants SET num = $num WHERE id = $participant_id";
                $this->db->query($sql);
            }
        }
        return $num;
    }

    public function checkNum($user_id, $group_id, $year_id, $num)
    {
        $sql = $this->numSql($group_id, $year_id, $num);
        $result = [];
        if ($this->db->select_cell($sql)) {
            return $result['error'] = 'Номер занят';
        }
        $result['update'] = $this->db->query("UPDATE mar_participants SET num = $num WHERE id = $user_id");
        return $result;
    }

    private function numSql($group_id, $year_id, $num = null)
    {
        $sql = "SELECT full_year FROM mar_groups WHERE id = $group_id";
        $age = $this->db->select_cell($sql);
        if ($age < 18) {
            $condition = ' < 18 ';
        } else {
            $condition = ' >= 18 ';
        }
        $sql = "SELECT mp.num FROM mar_participants mp 
                    LEFT JOIN mar_summary_table mst ON mp.id = mst.mar_participants_id
                    LEFT JOIN mar_groups mg ON mst.mar_groups_id = mg.id
                    WHERE mst.mar_year_table_id = $year_id
                    AND mg.full_year $condition 
                    ";

        if ($num) {
            $sql .= " AND mp.num = $num";
        }

        if (!$num) {
            $sql .= " AND mp.num IS NOT NULL ORDER BY mp.num";
        }

        return $sql;
    }

    public function setNumber(int $min, int $max, int $year_id, int $group_id, int $id)
    {
        if ($id > 0) {
            $this->db->query("DELETE FROM mar_numbers WHERE id = $id");
        }
        $sql = "INSERT INTO mar_numbers SET mar_year_table_id = $year_id, mar_groups_id = $group_id, min_num = $min, max_num = $max";
        $this->db->query($sql);
    }

    public function deleteItem(string $table, int $id)
    {
        $this->db->query("DELETE FROM $table WHERE id = $id");
    }

    public function checkNumber(int $min, int $max, int $year_id, int $id)
    {
        $sql = "DELETE FROM mar_numbers WHERE  id = $id";
        $this->db->query($sql);
        $sql = "SELECT * 
            FROM mar_numbers 
            WHERE mar_year_table_id = $year_id";

        $data = $this->db->select($sql);

        if ($data) {
            foreach ($data as $item) {
                if ($min >= $item['min_num'] && $min <= $item['max_num']) {
                    return ['error' => 'Неверное минимальное значение!'];
                }
                if ($max >= $item['min_num'] && $max <= $item['max_num']) {
                    return ['error' => 'Неверное максимальное значение!'];
                }
            }
        }

        return ['error' => false];
    }

    public function get_data_length_group_model($age, $sex)
    {
        $res[0] = $this->db->select_row("SELECT id, `length` 
                            FROM mar_groups 
                            WHERE `full_year` <= '$age' 
                            AND sex = '$sex'  ORDER BY full_year DESC LIMIT 1");
        if ($age >= 35) {
            $res[1] = $this->db->select_row("SELECT id, `length` 
                            FROM mar_groups 
                            WHERE `full_year` = '18' 
                            AND sex = '$sex'");
        }
        return $res;
    }


    public function check_dates_model()
    {
        $now = time();
        $c = $this->get_current_year();
        return $this->db->select_cell("SELECT id FROM mar_year_table 
                    WHERE date_stop > '$now' AND current='1'");
    }

    public function count_reg_model()
    {
        $c_child = $this->db->select_cell("SELECT count(mar_summary_table.id)
        FROM mar_summary_table
        JOIN mar_groups ON mar_summary_table.mar_groups_id = mar_groups.id
        JOIN mar_year_table ON mar_summary_table.mar_year_table_id = mar_year_table.id
        WHERE mar_groups.full_year < 18 AND mar_year_table.`current` = 1");

        $c_adults = $this->db->select_cell("SELECT count(mar_summary_table.id)
        FROM mar_summary_table
        JOIN mar_groups ON mar_summary_table.mar_groups_id = mar_groups.id
        JOIN mar_year_table ON mar_summary_table.mar_year_table_id = mar_year_table.id
        WHERE mar_groups.full_year >= 18 AND mar_year_table.`current` = 1");

        $res['child'] = $c_child;
        $res['adults'] = $c_adults;
        return $res;
    }

    public function get_group($year, $sex)
    {
        return $this->db->select_cell("SELECT id FROM mar_groups WHERE `full_year` <= '$year' 
                                  AND sex = '$sex'  ORDER BY full_year DESC LIMIT 1");
    }

    public function get_year($id = false, $date_birth = false)
    {
        if ($id) {
            $date_birth = $this->db->select_cell("SELECT date_birth FROM mar_participants WHERE id=?",
                array($id));
        }

        $cur = $this->get_current_year();

        return --$cur - substr($date_birth, -4, 4);
    }

    public function set_reg_model($arr, $num)
    {
        $year_id = $this->db->select_cell("SELECT id FROM mar_year_table WHERE current='1'");

        $id_p = $this->db->select_cell("SELECT id FROM mar_participants WHERE name='$arr[name]' AND date_birth='$arr[date_birth]'");

        if ($id_p) {
            $q = "SELECT id FROM mar_summary_table WHERE mar_year_table_id = '$year_id' AND mar_participants_id = '$id_p'";
            if ($this->db->select_cell($q)) {
                return 0;
            }
        }
        foreach ($arr as $k => $v) {
            if ($k != 'id_g') {
                $set[] = " `$k`='$v'";
            } else {
                $id_g = $v;
            }

        }
        $set = implode(',', $set);

        if (!$id_p) {
            $q = "INSERT INTO mar_participants SET $set";
            $id_p = $this->db->query($q);
        } else {
            $this->db->query("UPDATE mar_participants SET $set WHERE id = '$id_p'");
        }

        $q = "INSERT INTO mar_summary_table SET 
                mar_year_table_id='$year_id',
                mar_participants_id='$id_p',
                mar_groups_id='$id_g',
                num='$num'";
        $res = $this->db->query($q);

        if ($res) {
            $num = $this->getNumber($year_id, $id_g, $id_p);
        }

        return ['res' => $res, 'num' => !empty($num) ? $num : null];
    }

    public function max_num_model($full_year)
    {
        $s = $full_year < 18 ? '<' : '>=';
        $q = "SELECT
            MAX(mar_summary_table.num)
            FROM mar_summary_table
            JOIN mar_groups ON mar_summary_table.mar_groups_id = mar_groups.id
            WHERE mar_groups.full_year $s 18";
        return $this->db->select_cell($q);
    }


    public function del_model($arr)
    {
        $id_p = $arr['id'];
        $num = $arr['num'];
        $full_year = $arr['full_year'];

        $s = $full_year < 18 ? '<' : '>=';

        $q = "SELECT
            mar_summary_table.id AS id_s
            FROM mar_summary_table
            JOIN mar_groups ON mar_summary_table.mar_groups_id = mar_groups.id
            WHERE mar_summary_table.num > $num
            AND mar_groups.full_year $s 18";

        $t = $this->db->select($q);

        if ($t) {
            foreach ($t as $v) {
                $where[] = " id=$v[id_s] ";
            }
            $where = implode('OR', $where);
            $q = "UPDATE mar_summary_table SET num = num - 1 WHERE $where";
            $this->db->query($q);
        }

        $this->db->query("DELETE FROM mar_summary_table WHERE mar_participants_id=$id_p");
    }

    public function edit_data_model($arr)
    {
        $id = $arr['id'];
        $field = $arr['name_field'];
        $data = $arr[$field];
        $result = ['error' => false];
        if ($arr['name_field'] == 'sex') {
            $bool = true;
            $sex = mb_strtolower($data, 'UTF-8');
            if ($sex == 'м') {
                $sex = 'm';
            } elseif ($sex == 'ж') {
                $sex = 'w';
            } else {
                $bool = false;
            }
            if ($bool) {
                $year = $this->get_year($id);
                $id_g = $this->get_group($year, $sex);
                $this->db->query("UPDATE `mar_summary_table` SET 
                    `mar_groups_id`='$id_g' 
                    WHERE `mar_participants_id`='$id'");
            }
        } else {
            if ($arr['name_field'] == 'num') {
                // все данные по текущему году
                $year_data = $this->get_current_year(true);
                $age = $year = $this->get_year($id);
                if ($age < 18) {
                    $condition = ' < 18 ';
                } else {
                    $condition = ' >= 18 ';
                }
                $sql = "SELECT COUNT(mp.id) AS id 
                        FROM mar_participants mp 
                        LEFT JOIN mar_summary_table mst ON mp.id = mst.mar_participants_id
                        LEFT JOIN mar_groups mg ON mst.mar_groups_id = mg.id 
                        WHERE mst.mar_year_table_id = '$year_data[id]' 
                        AND mg.full_year $condition
                        AND mp.num = '$data'";
                if ($this->db->select_cell($sql)) {
                    $result['error'] = 'Номер занят!';
                }
            }
            if (!$result['error']) {
                $this->db->query("UPDATE `mar_participants` SET `$field`=? WHERE `id`=?", [$data, $id]);
            }
        }
        return $result;
    }

    public function menu_model()
    {
        $cur_year = $this->get_current_year();

        return $this->db->select("SELECT id, year, title FROM mar_year_table WHERE year <= '$cur_year' ORDER BY id DESC");
    }

    public function get_global_group($global_group, $year, $id)
    {
        $q = "SELECT
                mar_participants.id AS id_p,
                mar_participants.name,
                mar_participants.num,
                mar_participants.date_birth,
                mar_participants.region,
                mar_participants.email,
                mar_participants.phone,
                mar_participants.sports_category,
                mar_participants.district,
                mar_participants.org,
                mar_summary_table.num AS num_p,
                mar_groups.num AS num_g,
                mar_groups.`length`,
                mar_groups.sex,
                mar_groups.id AS group_id
                FROM
                mar_participants
                LEFT JOIN mar_summary_table ON mar_participants.id = mar_summary_table.mar_participants_id
                LEFT JOIN mar_year_table ON mar_summary_table.mar_year_table_id = mar_year_table.id
                LEFT JOIN mar_groups ON mar_summary_table.mar_groups_id = mar_groups.id
                WHERE mar_groups.full_year %s '18' AND mar_year_table.`year` = '%s' AND mar_year_table.id = $id
                ORDER BY
                mar_groups.num,
                mar_groups.sex,
                mar_participants.name;";

        $s = $global_group == 'child' ? '<' : '>=';

        $q = sprintf($q, $s, $year);

        return $this->db->select($q);
    }

    public function getTitle($id)
    {
        $q = "SELECT title FROM mar_year_table WHERE id = $id";
        return $this->db->select_cell($q);
    }


    public function set_length_model($id, $length)
    {
        $q = "UPDATE `mar_groups` SET `length`='$length' WHERE `id`='$id'";

        $this->db->query($q);
    }


    public function set_year_model($year, $date_stop, $is_new, $title, $is_copy_num)
    {
        $id = $this->db->select_cell("SELECT `id` FROM `mar_year_table` WHERE `current`='1'");
        if (!$is_new) {
            $q = "UPDATE `mar_year_table` SET date_stop='$date_stop', `title` = '$title' WHERE `id`=$id";
            $this->db->query($q);
        } else {
            $q = "UPDATE `mar_year_table` SET date_stop='', `current`='0' WHERE `id`=$id";
            $this->db->query($q);
            $q = "INSERT INTO `mar_year_table` SET `current`='1', date_stop='$date_stop', `year`='$year', `title`='$title'";
            $id = $this->db->query($q);
            if ($is_copy_num) {
                $q = "SELECT MAX(`mar_year_table_id`) FROM `mar_numbers`";
                $year_id = $this->db->select_cell($q);
                $q = "SELECT * FROM `mar_numbers` WHERE `mar_year_table_id` = '$year_id'";
                $data = $this->db->select($q);
                foreach ($data as $datum) {
                    $q = "INSERT INTO `mar_numbers` 
                        SET `mar_year_table_id` = $id,
                        `mar_groups_id` = $datum[mar_groups_id],
                        `min_num` = $datum[min_num],
                        `max_num` = $datum[max_num]";
                    $this->db->query($q);
                }
            }
        }
    }

    public function get_current_year($date_stop = false)
    {
        if (!$date_stop) {
            return $this->db->select_cell("SELECT `year` FROM `mar_year_table` WHERE `current` = '1'");
        }
        return $this->db->select_row("SELECT `id`, `year`, `date_stop`, `title` FROM `mar_year_table` WHERE `current` = '1'");
    }

    public function get_all_groups_model()
    {
        $q = "SELECT * FROM `mar_groups`  ORDER BY  `full_year`, `sex`";
        $arr = $this->db->select($q);

        $prev_year = $this->get_current_year() - 1;

        foreach ($arr as $v) {
            if ($v['full_year'] < 18) {
                $group_year = 'child';
                $sex = $v['sex'] == 'm' ? 'boys' : 'girls';
            }

            if ($v['full_year'] >= 18) {
                $group_year = 'adults';
                $sex = $v['sex'] == 'm' ? 'men' : 'woomen';
            }

            $all_mar_groups[$group_year][$sex][] = array(
                'num' => $v['num'],
                'id' => $v['id'],
                'full_year' => $v['full_year'],
                'length' => $v['length']
            );
        }

        foreach ($all_mar_groups as $group_year_name => $data) {
            $name_group = array_keys($data);

            $res[$group_year_name] = [];

            foreach ($data[$name_group[0]] as $k => $v) {
                $start_age = $v['full_year'];

                if (isset($data[$name_group[0]][$k + 1])) {
                    $end_age = $data[$name_group[0]][$k + 1]['full_year'] - 1;
                } else {
                    $end_age = $group_year_name == 'child' ? 17 : '';
                }

                $start_year = $prev_year - $start_age;

                $end_year = $end_age ? $prev_year - $end_age : '';

                $distanse = [];

                $distanse[$name_group[0]] = ['id' => $v['id'], 'length' => $v['length']];

                if (isset($data[$name_group[1]][$k])) {
                    $distanse[$name_group[1]] = [
                        'id' => $data[$name_group[1]][$k]['id'],
                        'length' => $data[$name_group[1]][$k]['length']
                    ];
                } else {
                    $distanse[$name_group[1]] = [];
                }

                $res[$group_year_name][] = [
                    'group_id' => $v['id'],
                    'num' => $v['num'],
                    'start_age' => $start_age,
                    'end_age' => $end_age,
                    'start_year' => $start_year,
                    'end_year' => $end_year,
                    'distanse' => $distanse
                ];
            }
        }

        return $res;
    }

    public function getNumbers(int $year_id)
    {
        $sql = "SELECT * FROM mar_numbers WHERE mar_year_table_id = $year_id";
        $result = [];
        $arr = $this->db->select($sql);
        if (is_array($arr)) {
            foreach ($arr as $item) {
                $result[$item['mar_year_table_id']][$item['mar_groups_id']]['id'] = $item['id'];
                $result[$item['mar_year_table_id']][$item['mar_groups_id']]['min'] = $item['min_num'];
                $result[$item['mar_year_table_id']][$item['mar_groups_id']]['max'] = $item['max_num'];
            }
        }
        return $result;
    }
}
